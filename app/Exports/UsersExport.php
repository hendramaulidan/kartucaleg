<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
// use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection,WithHeadings
{
    // use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */

    public function headings(): array
    {
        return [
          
            'Nomor Whatsapp',
         
        ];
    }


    public function collection()
    {
        return User::join('user_public','users.id','=','user_public.user_id')
        ->where('users.role','0')
        ->select('user_public.no_hp')
        ->get();
        // return User::all();
    }
}
