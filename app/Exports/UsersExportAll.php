<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExportAll implements FromCollection,WithHeadings
{
    // use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */


    public function headings(): array
    {
        return [
            'Nama',
            'Email',
            'Nomor Whatsapp',
            'Alamat',
            'TPS',
        ];
    }

    public function collection()
    {
        return User::join('user_public','users.id','=','user_public.user_id')
        ->where('users.role','0')
        ->select('users.name','users.email','user_public.no_hp','user_public.alamat','user_public.tps')
        ->get();
        // return User::all();
    }
}
