<?php

namespace App\Http\Controllers;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport; // Sesuaikan dengan model Anda
use App\Models\Bantuan;
use App\Models\BrosurDigital;
use App\Models\DapilKota;
use App\Models\DokKartuCaleg;
use App\Models\DokMarketing;
use App\Models\JenisToko;
use App\Models\JenisUsaha;
use App\Models\Merchan;
use App\Models\PPOB;
use App\Models\ProgramMenarik;
use App\Models\Province;
use App\Models\Tema;
use App\Models\Toko;
use App\Models\User;
use App\Models\UserWebsite;
use App\Models\UserWebsitePrem;
use App\Models\Webinar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function dashboard()
    {
        $data['config'] = \App\Models\DataConfig::first();
        $data['jumlah_user'] = User::where('role',0)->count();
        $data['user_website'] = UserWebsite::where('status',0)->count();
        $data['user_website_prem'] = UserWebsitePrem::where('status',0)->count();
        $data['user_ppob'] = PPOB::where('status',0)->count();
        $data['user_brosur'] = BrosurDigital::where('status',0)->count();
        $data['user_merchant'] = UserWebsite::where('status',0)->count();
        $data['user_webinar'] = Webinar::where('status',0)->count();
        $data['user_bantuan'] = ProgramMenarik::where('status',0)->count();

        //proses
        $data['user_website_proses'] = UserWebsite::where('status',1)->count();
        $data['user_website_prem_proses'] = UserWebsitePrem::where('status',1)->count();
        $data['user_ppob_proses'] = PPOB::where('status',1)->count();
        $data['user_brosur_proses'] = BrosurDigital::where('status',1)->count();
        $data['user_merchant_proses'] = UserWebsite::where('status',1)->count();
        $data['user_webinar_proses'] = Webinar::where('status',1)->count();
        $data['user_bantuan_proses'] = ProgramMenarik::where('status',1)->count();
        $data['user_website_proses'] = UserWebsite::where('status',1)->count();

        //selesai
        $data['user_website_selesai'] = UserWebsite::where('status',1)->count();
        $data['user_website_prem_selesai'] = UserWebsitePrem::where('status',2)->count();
        $data['user_ppob_selesai'] = PPOB::where('status',2)->count();
        $data['user_brosur_selesai'] = BrosurDigital::where('status',2)->count();
        $data['user_merchant_selesai'] = UserWebsite::where('status',2)->count();
        $data['user_webinar_selesai'] = Webinar::where('status',2)->count();
        $data['user_bantuan_selesai'] = ProgramMenarik::where('status',2)->count();
        
        $data['bantuan'] = Bantuan::get();
        return view("dashboard",$data);
    }
    public function listUser()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("list-user",$data);
    }
    public function webRequest()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("web-request",$data);
    }
    public function webRequestPrem()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("web-request-prem",$data);
    }
    public function brosurDigital()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("brosur-digital",$data);
    }
    public function detailUser($id)
    {
        $data['user'] = User::find($id);
        $data['config'] = \App\Models\DataConfig::first();
        return view("detailUser",$data);
    }

    public function merchan()
    {$data['config'] = \App\Models\DataConfig::first();
        return view("merchan",$data);
    }

    public function ppob()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("ppob",$data);
    }

    public function webinar()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("webinar",$data);
    }

    public function programMenarik()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view("program-menarik",$data);
    }


    function dapilKota(){
        $data['config'] = \App\Models\DataConfig::first();
        $data['provinsi'] = Province::get();
        return view("fiturAdmin.dapilKota",$data);
    }
    function jenisToko(){
        $data['config'] = \App\Models\DataConfig::first();
        return view("fiturAdmin.jenisToko",$data);
    }
    function jenisUsaha(){
        $data['config'] = \App\Models\DataConfig::first();
        return view("fiturAdmin.jenisUsaha",$data);
    }
    function toko(){
        $data['config'] = \App\Models\DataConfig::first();
        $data['kota'] = DapilKota::get();
        $data['jenis'] = jenisToko::get();
        return view("fiturAdmin.toko",$data);
    }


    function jenisBantuan(){
        $data['config'] = \App\Models\DataConfig::first();
        return view("fiturAdmin.bantuan",$data);
    }
    function temaWork(){
        $data['config'] = \App\Models\DataConfig::first();
     
        return view("fiturAdmin.tema",$data);
    }


    function kartuCaleg() {
        $data['config'] = \App\Models\DataConfig::first();
     
        return view("dokumentasi.kartuCaleg",$data);
    }
    function marketing() {
        $data['config'] = \App\Models\DataConfig::first();
     
        return view("dokumentasi.marketing",$data);
    }



    function tambahDapilKota(Request $req){

        $validatedData = $req->validate([
            'provinsi' => ['required'],
            'kota' => ['required'],
            'password' => ['required'],
        ]);
        if ($req->password != "yecepe210581") {
            return redirect()->back()->with('error','Password yang anda masukan salah');
        }
        $kota = strtolower($req->input('kota'));
        $kota = ucwords($kota);
       DapilKota::insert([
        'kota'=>$kota
       ]);


        return redirect()->back()->with('success','berhasil menambah Kota');
    }
    function tambahJenisToko(Request $req){
        
        $validatedData = $req->validate([
           
            'jenis_toko' => ['required'],
        ]);
      
       JenisToko::insert([
        'jenis'=> $req->jenis_toko
       ]);



        return redirect()->back()->with('success','berhasil menambah Jenis Toko');
    }


    function tambahJenisUsaha(Request $req){
        
        $validatedData = $req->validate([
           
            'jenis_usaha' => ['required'],
        ]);
      
       JenisUsaha::insert([
        'usaha'=> $req->jenis_usaha
       ]);



        return redirect()->back()->with('success','berhasil menambah Jenis Usaha');
    }

    function tambahTemaWork(Request $req){
        
        $validatedData = $req->validate([
           
            'tema' => ['required'],
            'tipe' => ['required'],
        ]);
      
       Tema::insert([
        'tema'=> $req->tema,
        'tipe'=> $req->tipe,
       ]);



        return redirect()->back()->with('success','berhasil menambah Jenis Tema');
    }

    function tambahJenisBantuan(Request $req){
        
        $validatedData = $req->validate([
           
            'bantuan' => ['required'],
            
        ]);
      
       Bantuan::insert([
        'bantuan'=> $req->bantuan,
      
       ]);



        return redirect()->back()->with('success','berhasil menambah bantuan');
    }
    function tambahDokumentasiKartuCaleg(Request $request){
        
      // Validate the form inputs if needed
        // return $request;   

        if (!$request->hasFile('foto')) {
            return redirect()->back()->with('error', 'Foto Dokumentasi Wajib di isi');
        }

        $validatedData = $request->validate([
            'alamat' => 'required',
            'deskripsi' => 'required',
        ]);
        // Process the form submission and store the data
        $dok_kartucaleg = new DokKartuCaleg;
        $dok_kartucaleg->alamat = $request->input('alamat');
        $dok_kartucaleg->deskripsi = $request->input('deskripsi');
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $pathFotoArray = [];
            foreach ($foto as $foto) {
                $pathFoto = $foto->store('foto_dokumentasi', 'public');
                $pathFotoArray[] = $pathFoto;
            }
            $pathFotoString = implode('|', $pathFotoArray);
            $dok_kartucaleg->foto = $pathFotoString;
        }
        $dok_kartucaleg->save();
        // Redirect back to the previous page
        return redirect()->back()->with('success_create','Berhasil mengirim data dokumentasi');
    }
    function tambahDokumentasiMarketing(Request $request){
        
      // Validate the form inputs if needed
        // return $request;   

        if (!$request->hasFile('foto')) {
            return redirect()->back()->with('error', 'Foto Dokumentasi Wajib di isi');
        }

        $validatedData = $request->validate([
            'alamat' => 'required',
            'deskripsi' => 'required',
        ]);
        // Process the form submission and store the data
        $dok_marketing = new DokMarketing();
        $dok_marketing->alamat = $request->input('alamat');
        $dok_marketing->deskripsi = $request->input('deskripsi');
        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $pathFotoArray = [];
            foreach ($foto as $foto) {
                $pathFoto = $foto->store('foto_dokumentasi_marketing', 'public');
                $pathFotoArray[] = $pathFoto;
            }
            $pathFotoString = implode('|', $pathFotoArray);
            $dok_marketing->foto = $pathFotoString;
        }
        $dok_marketing->save();
        // Redirect back to the previous page
        return redirect()->back()->with('success_create','Berhasil mengirim data dokumentasi');
    }


    function tambahToko(Request $req){
        $validatedData = $req->validate([
           
            'toko' => ['required'],
            'id_kota' => ['required'],
            'id_jenis' => ['required'],
        ]);
    //   return $req;
       Toko::insert([
        'toko'=> $req->toko,
        'id_kota'=> $req->id_kota,
        'id_jenis'=> $req->id_jenis,
       ]);


        return redirect()->back()->with('success','berhasil menambah Kota');
    }



    function hapusDapilKota(Request $req){
        $validatedData = $req->validate([
            'password' => ['required'],
        ]);

        if ($req->password != "yecepe210581") {
            return redirect()->back()->with('error','Password yang anda masukan salah');
        }

        DapilKota::where('id',$req->id)->delete();
        return redirect()->back()->with('success','berhasil menghapus Kota');
    }
    function hapusJenisToko($id){
        JenisToko::where('id',$id)->delete();
        return redirect()->back()->with('success','berhasil menghapus Jenis Toko');
    }
    function hapusToko($id){
        Toko::where('id',$id)->delete();
        return redirect()->back()->with('success','berhasil menghapus Toko');
    }
    function hapusJenisUsaha($id){
        JenisUsaha::where('id',$id)->delete();
        return redirect()->back()->with('success','berhasil menghapus jenis usaha');
    }

    function hapusTemaWork($id){
        Tema::where('id',$id)->delete();
        return redirect()->back()->with('success','berhasil menghapus tema');
    }
    function hapusJenisBantuan($id){
        Bantuan::where('id',$id)->delete();
        return redirect()->back()->with('success','berhasil menghapus bantuan');
    }
    function updateReqWeb(Request $req,$id){
        UserWebsite::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status pembuatan Website');
    }
    function updateReqWebPremium(Request $req,$id){
        UserWebsitePrem::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status pembuatan Website');
    }
    function updateBrosur(Request $req,$id){
        BrosurDigital::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status brosur digital');
    }
    function updateMerchan(Request $req,$id){
        Merchan::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status Merchan');
    }
    function updatePpob(Request $req,$id){
        PPOB::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status PPOB');
    }
    function updateWebinar(Request $req,$id){
        Webinar::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status Webinar');
    }
    function updateBantuan(Request $req,$id){
        ProgramMenarik::where('id',$id)->update([
            'status'=>$req->status
        ]);
        return redirect()->back()->with('success','berhasil mengupdate status Webinar');
    }


    function unduhData(){
        return Excel::download(new UsersExport, 'users.xlsx');
    }



}
