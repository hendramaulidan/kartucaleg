<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function authenticated(Request $request, $user)
    {
        if (auth()->user()->role == 'admin') {
            return redirect('/admin/home');
        }else{
            return redirect('/claim');
        }
      
    }

    public function login(Request $request): RedirectResponse
    {
        // $input = $request->all();

        // $this->validate($request, [
        //     'email' => 'required|email',
        //     'password' => 'required',
        // ]);


        // if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password'])))
        // {
        //     if (auth()->user()->role == 'admin') {
        //         return redirect('/admin/home');
        //     }else{
        //         return redirect('/home');
        //     }
        // }else{
        //     return redirect()->route('login')

        //     ->with('error','Email-Address And Password Are Wrong.');
        // }




        $credentials = $request->only('login', 'password');
        $username = $credentials['login'];

        // Periksa jika input adalah email atau kode_kartu
        if (filter_var($username, FILTER_VALIDATE_EMAIL) === false) {
            // Cari pengguna berdasarkan kode_kartu
            $user = User::where('kode_kartu', $username)->first();
        } else {
            // Cari pengguna berdasarkan email
            $user = User::where('email', $username)->first();
        }

        if ($user && Auth::attempt(['email' => $user->email, 'password' => $credentials['password']])) {
            if (auth()->user()->role == 'admin') {
                return redirect('/admin/home');
            } else {
                return redirect('/claim');
            }
        } else {
            return redirect()->back()

                ->with('errorLogin', 'Nomor Seri anda atau password anda salah');
        }
    }
}
