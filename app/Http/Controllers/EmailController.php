<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendWelcomeEmail(Request $request)
    {
        $data = [
            'name' => 'Hendra Maulidan',
            // tambahkan data lain yang ingin Anda gunakan dalam tampilan email
        ];
        $email = $request->email;

        Mail::send('emails.welcome', $data, function ($message) use ($email){
            $message->to($email, 'Recipient Name')
                ->subject('Welcome to My Website');
        });

        return redirect()->back()->with('success','silahkan cek email anda');
    }
}
