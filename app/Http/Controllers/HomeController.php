<?php

namespace App\Http\Controllers;

use App\Models\BrosurDigital;
use App\Models\DapilKota;
use App\Models\JenisToko;
use App\Models\JenisUsaha;
use App\Models\Merchan;
use App\Models\PPOB;
use App\Models\ProgramMenarik;
use App\Models\Tema;
use App\Models\Toko;
use App\Models\UserWebsite;
use App\Models\UserWebsitePrem;
use App\Models\Webinar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the claim form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function claim()
    {
        $userId = Auth::id();
        $existingData = UserWebsite::where('user_id', $userId)->first();
        $webinarKosong = Webinar::where('user_id', $userId)->first();
        $ppobKosong = PPOB::where('user_id', $userId)->first();
        $programMenarikKosong = ProgramMenarik::where('user_id', $userId)->first();
        $merchanKosong = Merchan::where('user_id', $userId)->first();
        $BrosurKosong = BrosurDigital::where('user_id', $userId)->first();
        $config = \App\Models\DataConfig::first();
        $dapil_kota = DapilKota::get();
        $jenis_toko = JenisToko::get();
        $toko = Toko::get();
        $jenis_usaha = JenisUsaha::get();
        return view('claim', compact('existingData','webinarKosong','ppobKosong','programMenarikKosong','merchanKosong','BrosurKosong','config','dapil_kota', 'jenis_toko', 'toko', 'jenis_usaha'));
    }

    /**
     * Store the submitted claim.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // Validate the form inputs if needed
        // return $request;   
        $validatedData = $request->validate([
            'subdomain' => 'required',
            'model-usaha' => 'required',
            'alamat-usaha' => 'required',
            'deskripsi' => 'required',
            // 'foto.*' => 'required|image',
        ]);
    
        // Retrieve the authenticated user's ID
        $userId = Auth::id();

        // Check if the user already has existing data
        $existingData = UserWebsite::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Anda sudah pernah mengupload.');
        }
    
    
        // Process the form submission and store the data
        $userWebsite = new UserWebsite;
        $userWebsite->user_id = $userId;
        $userWebsite->alamat_usaha = $request->input('alamat-usaha');
        $userWebsite->subdomain = $request->input('subdomain');
        $userWebsite->deskripsi = $request->input('deskripsi');
        $userWebsite->jenis_usaha = $request->input('jenis-usaha');
        $userWebsite->model_usaha = $request->input('model-usaha');
        $userWebsite->status = $request->input('status', 0);
    
        // // Upload the image files
        // $photoFilenames = [];
        // if ($request->hasFile('foto')) {
        //     foreach ($request->file('foto') as $foto) {
        //         $fotoName = time() . '_' . $foto->getClientOriginalName();
        //         $foto->move(public_path('photos'), $fotoName);
        //         $photoFilenames[] = $fotoName;
        //     }
        // }
    
        // // Concatenate the filenames into a comma-separated string
        // $userWebsite->foto = implode(',', $photoFilenames);
    
        $userWebsite->save();
    
        // Flash the success message to the session
        Session::flash('success_create', 'Berhasil mengirim data website');
    
        // Redirect back to the previous page
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk brosur');
    }

    public function storePrem(Request $request)
    {
        // Validate the form inputs if needed
        // return $request;   
        $validatedData = $request->validate([
            'domain' => 'required',
            'model-usaha' => 'required',
            'alamat-usaha' => 'required',
            'deskripsi' => 'required',
            // 'foto.*' => 'required|image',
        ]);
    
        // Retrieve the authenticated user's ID
        $userId = Auth::id();

        // Check if the user already has existing data
        $existingData = UserWebsitePrem::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Anda sudah pernah mengupload.');
        }
    
    
        // Process the form submission and store the data
        $userWebsite = new UserWebsitePrem;
        $userWebsite->user_id = $userId;
        $userWebsite->alamat_usaha = $request->input('alamat-usaha');
        $userWebsite->domain = $request->input('domain');
        $userWebsite->deskripsi = $request->input('deskripsi');
        $userWebsite->jenis_usaha = $request->input('jenis-usaha');
        $userWebsite->model_usaha = $request->input('model-usaha');
        $userWebsite->status = $request->input('status', 0);
    
        // // Upload the image files
        // $photoFilenames = [];
        // if ($request->hasFile('foto')) {
        //     foreach ($request->file('foto') as $foto) {
        //         $fotoName = time() . '_' . $foto->getClientOriginalName();
        //         $foto->move(public_path('photos'), $fotoName);
        //         $photoFilenames[] = $fotoName;
        //     }
        // }
    
        // // Concatenate the filenames into a comma-separated string
        // $userWebsite->foto = implode(',', $photoFilenames);
    
        $userWebsite->save();
    
        // Flash the success message to the session
        Session::flash('success_create', 'Berhasil mengirim data website');
    
        // Redirect back to the previous page
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk brosur');
    }


    public function brosur(Request $request)
    {
        // Validate the form inputs if needed
        // return $request;   

        if (!$request->hasFile('foto_usaha')) {
            return redirect()->back()->with('error', 'Foto Usaha Wajib di isi');
        }

        $validatedData = $request->validate([
            'model-usaha' => 'required',
            'jenis-usaha' => 'required',
            'alamat-usaha' => 'required',
            'deskripsi' => 'required',
            // 'foto.*' => 'required|image',
        ]);
    
        // Retrieve the authenticated user's ID
        $userId = Auth::id();

        // Check if the user already has existing data
        $existingData = BrosurDigital::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Anda sudah pernah mengupload.');
        }
    
    
        // Process the form submission and store the data
        $userWebsite = new BrosurDigital;
        $userWebsite->user_id = $userId;
        $userWebsite->alamat_usaha = $request->input('alamat-usaha');
    
     
        $userWebsite->deskripsi = $request->input('deskripsi');
        $userWebsite->jenis_usaha = $request->input('jenis-usaha');
        $userWebsite->model_usaha = $request->input('model-usaha');
    
        if ($request->hasFile('foto_usaha')) {
            $foto_usaha = $request->file('foto_usaha');
            $pathFotoArray = [];
            foreach ($foto_usaha as $foto) {
                
                $pathFoto = $foto->store('foto_usaha', 'public');
                $pathFotoArray[] = $pathFoto;
            }
            $pathFotoString = implode('|', $pathFotoArray);
            $userWebsite->foto_usaha = $pathFotoString;
        }
    
        $userWebsite->save();
    
        // Flash the success message to the session
        Session::flash('success', 'Berhasil mengirim data brosur');
    
        // Redirect back to the previous page
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk brosur');
    }

    public function ppob(Request $request)
    {  
             $request->validate([
          
            'nama' => 'required',
            'email' => 'required',
            'no_hp' => 'required',
        ]);  
        $userId = Auth::id();
        $existingData = PPOB::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Anda sudah pernah mengupload.');
        }
       // Process the form submission and store the data
       $ppob = new PPOB;
       $ppob->user_id = $userId;
       $ppob->nama = $request->input('nama');
       $ppob->email = $request->input('email');
       $ppob->no_hp = $request->input('no_hp');
   
      
   
       $ppob->save();
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk PPOB');
    }
   
    public function merchan(Request $request)
    {  
        $request->validate([
            'dapil' => 'required',
            'jenis_toko' => 'required',
            'toko' => 'required',
            'nama' => 'required',
            'nomor_kartu' => 'required',
        ]);  
        $userId = Auth::id();
        $existingData = Merchan::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Kamu sudah pernah mengupload.');
        }
        // Process the form submission and store the data
        $merchan = new Merchan();
        $merchan->user_id = $userId;
        $merchan->dapil = explode('|', $request->input('dapil'))[1];
        $merchan->jenis_toko = explode('|', $request->input('jenis_toko'))[1];
        $merchan->toko = explode('|', $request->input('toko'))[1];
        $merchan->nama = $request->input('nama');
        $merchan->nomor_kartu = $request->input('nomor_kartu');
        
        
        
        $merchan->save();
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk merchandise');
    }
    public function webinar(Request $request)
    {  
        $request->validate([
            'dapil' => 'required',
            'tema' => 'required',
            'nama' => 'required',
            'nomor_kartu' => 'required',
        ]);  
        $userId = Auth::id();
        $existingData = Webinar::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Kamu sudah pernah mengupload.');
        }
        // Process the form submission and store the data
        $webinar = new Webinar();
        $webinar->user_id = $userId;
        $webinar->dapil = $request->input('dapil');
        $webinar->tema = $request->input('tema');
        $webinar->nama = $request->input('nama');
        $webinar->nomor_kartu = $request->input('nomor_kartu');
        $webinar->tipe = $request->input('workshop');
        
        $webinar->save();
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk webinar');
    }
    public function programMenarik(Request $request)
    {  
        $request->validate([
            'dapil' => 'required',
            'jenis_bantuan' => 'required',
            'nama' => 'required',
            'nomor_kartu' => 'required',
        ]);  
        $userId = Auth::id();
        $existingData = ProgramMenarik::where('user_id', $userId)->first();
        if ($existingData) {
            // User already has existing data
            // Redirect back with an error message
            return redirect()->back()->with('error', 'Kamu sudah pernah mengupload.');
        }
        // Process the form submission and store the data
        $program_menarik = new ProgramMenarik;
        $program_menarik->user_id = $userId;
        $program_menarik->dapil = $request->input('dapil');
        $program_menarik->jenis_bantuan = $request->input('jenis_bantuan');
        $program_menarik->nama = $request->input('nama');
        $program_menarik->nomor_kartu = $request->input('nomor_kartu');
        
        
        $program_menarik->save();
        return redirect()->back()->with('success_create','Berhasil mengirim data untuk program menarik');
    }

    public function getJenis(Request $request)
    {
        $data = Toko::where('id_jenis', $request->idJt)->where('id_kota', $request->idDapil)->get();
        return response()->json($data, 200);
    }

    public function getTema(Request $request)
    {
        $data = Tema::where('tipe', $request->idJw)->get();
        return response()->json($data, 200);
    }

}
