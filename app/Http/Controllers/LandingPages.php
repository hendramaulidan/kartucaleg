<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\User;
use App\Models\UserPublic;
use App\Models\Village;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;

class LandingPages extends Controller
{
    public function index()
    {
        if (auth()->check()) {
            if (auth()->user()->role == 'admin') {
                return redirect('/admin/home');
            } else {
                return redirect('/claim');
            }
        }


        $data['provinsi'] = Province::get();
        $data['config'] = \App\Models\DataConfig::first();
        return view('landing', $data);
    }

    public function home()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view('home.index', $data);
    }


    public function broadcast()
    {
        $data['config'] = \App\Models\DataConfig::first();
        return view('home.broadcast', $data);
    }

    public function getKota($id)
    {
        $data = Regency::where('province_id', $id)->get();
        return response()->json($data, 200);
    }
    public function getKec($id)
    {
        $data = District::where('regency_id', $id)->get();
        return response()->json($data, 200);
    }

    public function getKel($id)
    {
        $data = Village::where('district_id', $id)->get();
        return response()->json($data, 200);
    }
    function simpanPendaftaran(Request $request)
    {
        if (!$request->hasFile('ktp')) {
            return redirect()->back()->with('error', 'Foto KTP Wajib di isi');
        }
       
        // jika bersedia
        $koordinator = null;
        if ($request->input('bersedia') != null) {
            $request->validate([
                'koordinator' => 'required',
            ]);
            $koordinator = $request->input('koordinator');
        }

        $validatedData = $request->validate([
            'name' => ['required'],
            'kode_kartu' => ['required',Rule::unique('users','kode_kartu')],
            'no_hp' => ['required'],
            'email' => [['required'],'email',Rule::unique('users','email')],
            'password' => ['required'],
            'alamat' => ['required'],
            'tps' => ['required','numeric'],
            'ktp' => ['nullable','file'],
        ]);



        $provinsi =  explode("|", $request->input('provinsi'))[1];
        $kota =  explode("|", $request->input('kota'))[1];
        $kecamatan =  explode("|", $request->input('kecamatan'))[1];
        $kelurahan =  explode("|", $request->input('kelurahan'))[1];

        $alamat = $request->input('alamat') . ', ' .
            $provinsi . ', ' .
            $kota  . ', ' .
            $kecamatan . ', ' .
            $kelurahan . ', ' .
            'RT ' . $request->input('rt') . ' / RW ' . $request->input('rw');

        $data = new  User();
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->kode_kartu = $request->input('kode_kartu');
        $data->role = 0;
        $data->password = bcrypt($request->input('password'));
        // $data->no_hp = $request->input('no_hp');
        // $data->alamat = $alamat;
        $data->save();
        $newDataid = $data->id;
        $pubData = new  UserPublic();
        $pubData->user_id =  $newDataid;
        $pubData->no_hp = $request->input('no_hp');
        // $pubData->no_kk = $request->input('no_kk');
        // $pubData->no_ktp = $request->input('no_ktp');
        $pubData->kode_kartu = $request->input('kode_kartu');
        $pubData->alamat = $alamat;
        $pubData->tps = $request->input('tps');
        $pubData->koordinator = $koordinator;
        if ($request->hasFile('ktp')) {
            $ktp = $request->file('ktp');
            $ktpPath = $ktp->store('ktp', 'public');
            $pubData->foto_ktp = $ktpPath;
        }
        $pubData->save();
        return redirect()->back()->with('success', 'Data berhasil disimpan  ');
        //dan silahkan cek email anda.
    }
}
