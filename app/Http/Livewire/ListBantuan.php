<?php

namespace App\Http\Livewire;

use App\Models\Bantuan;
use App\Models\DapilKota;
use Livewire\WithPagination;
use Livewire\Component;

class ListBantuan extends Component
{
    use WithPagination;
    public function render()
    {
        $users = Bantuan::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-bantuan',compact('users','config'));
    }
}
