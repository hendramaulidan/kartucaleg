<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\BrosurDigital;
use Livewire\WithPagination;
use Livewire\Component;

class ListBrosurDigital extends Component
{
    use WithPagination;
    public function render()
    {
        $config = \App\Models\DataConfig::first();
        $users = BrosurDigital::paginate(15);
        return view('livewire.list-brosur-digital',compact('users','config'));
    }
}
