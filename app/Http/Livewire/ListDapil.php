<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use Livewire\WithPagination;
use Livewire\Component;

class ListDapil extends Component
{
    use WithPagination;
    public function render()
    {
        $users = DapilKota::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-dapil',compact('users','config'));
    }
}
