<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\JenisToko;
use Livewire\WithPagination;
use Livewire\Component;

class ListJenisToko extends Component
{
    use WithPagination;
    public function render()
    {
        $users = JenisToko::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-jenis-toko',compact('users','config'));
    }
}
