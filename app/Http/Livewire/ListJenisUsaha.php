<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\JenisUsaha;
use App\Models\Toko;
use Livewire\WithPagination;
use Livewire\Component;

class ListJenisUsaha extends Component
{
    use WithPagination;
    public function render()
    {
        $users = JenisUsaha::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-jenis-usaha',compact('users','config'));
    }
}
