<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\DokKartuCaleg;
use App\Models\Tema;
use App\Models\Toko;
use Livewire\WithPagination;
use Livewire\Component;

class ListKartuCaleg extends Component
{
    use WithPagination;
    public function render()
    {
        $users = DokKartuCaleg::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-kartu-caleg',compact('users','config'));
    }
}
