<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\DokMarketing;
use App\Models\Tema;
use App\Models\Toko;
use Livewire\WithPagination;
use Livewire\Component;

class ListMarketing extends Component
{
    use WithPagination;
    public function render()
    {
        $users = DokMarketing::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-marketing',compact('users','config'));
    }
}
