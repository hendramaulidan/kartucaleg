<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Merchan;
use Livewire\WithPagination;
use Livewire\Component;

class ListMerchan extends Component
{
    use WithPagination;
    public function render()
    {
        $users = Merchan::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-merchan',compact('users','config'));
    }
}
