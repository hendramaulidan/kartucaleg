<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\PPOB;
use Livewire\WithPagination;
use Livewire\Component;

class ListPPOB extends Component
{
    use WithPagination;
    public function render()
    {
        $users = PPOB::paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-p-p-o-b',compact('users','config'));
    }
}
