<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Merchan;
use App\Models\ProgramMenarik;
use Livewire\WithPagination;
use Livewire\Component;

class ListProgramMenarik extends Component
{
    use WithPagination;
    public function render()
    {
        $users = ProgramMenarik::paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-program-menarik',compact('users','config'));
    }
}
