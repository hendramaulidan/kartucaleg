<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserWebsite;
use Livewire\WithPagination;
use Livewire\Component;

class ListRequestWebsite extends Component
{
    use WithPagination;
    public function render()
    {
        $users = UserWebsite::paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-request-website',compact('users','config'));
    }
}
