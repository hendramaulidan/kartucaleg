<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\UserWebsite;
use App\Models\UserWebsitePrem;
use Livewire\WithPagination;
use Livewire\Component;

class ListRequestWebsitePremium extends Component
{
    use WithPagination;
    public function render()
    {
        $users = UserWebsitePrem::paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-request-website-premium',compact('users','config'));
    }
}
