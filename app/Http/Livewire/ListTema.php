<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\Tema;
use App\Models\Toko;
use Livewire\WithPagination;
use Livewire\Component;

class ListTema extends Component
{
    use WithPagination;
    public function render()
    {
        $users = Tema::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-tema',compact('users','config'));
    }
}
