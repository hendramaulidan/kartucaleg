<?php

namespace App\Http\Livewire;

use App\Models\DapilKota;
use App\Models\Toko;
use Livewire\WithPagination;
use Livewire\Component;

class ListToko extends Component
{
    use WithPagination;
    public function render()
    {
        $users = Toko::paginate(15);
        $config= \App\Models\DataConfig::first();
        return view('livewire.list-toko',compact('users','config'));
    }
}
