<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\WithPagination;
use Livewire\Component;

class ListUserData extends Component
{
    use WithPagination;
    public function render()
    {
        $users = User::where('role',0)->paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-user-data',compact('users','config'));
    }
}
