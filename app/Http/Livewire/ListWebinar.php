<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Webinar;
use Livewire\WithPagination;
use Livewire\Component;

class ListWebinar extends Component
{
    use WithPagination;
    public function render()
    {
        $users = Webinar::paginate(15);
        $config = \App\Models\DataConfig::first();
        return view('livewire.list-webinar',compact('users','config'));
    }
}
