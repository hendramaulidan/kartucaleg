<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrosurDigital extends Model
{
    use HasFactory;
    protected $table = 'brosur_digital';
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
