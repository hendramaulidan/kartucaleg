<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DapilKota extends Model
{
    use HasFactory;
    protected $table = 'dapil_kota';
}
