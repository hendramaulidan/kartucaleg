<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokMarketing extends Model
{
    use HasFactory;
    protected $table = "dok_marketing";
}
