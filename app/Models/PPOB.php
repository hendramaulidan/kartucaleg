<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PPOB extends Model
{
    use HasFactory;
    protected $table = 'ppob';
}
