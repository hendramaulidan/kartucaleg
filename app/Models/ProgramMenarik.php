<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramMenarik extends Model
{
    use HasFactory;
    protected $table = 'program_menarik';
}
