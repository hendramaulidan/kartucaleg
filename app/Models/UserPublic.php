<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPublic extends Model
{
    use HasFactory;
    protected $table = "user_public";
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
