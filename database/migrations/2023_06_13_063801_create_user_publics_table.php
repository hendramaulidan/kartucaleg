<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_public', function (Blueprint $table) {
            $table->id();
            $table->string('user_id',20);
            $table->string('no_hp',14);
            $table->string('no_ktp',50);
            $table->string('no_kk',50);
            $table->string('foto_ktp',);
            // $table->string('foto_kk');
            $table->string('kode_kartu');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_public');
    }
};
