<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('brosur_digital', function (Blueprint $table) {
            $table->id();
            $table->string('user_id',90);
            $table->string('model-usaha',90);
            $table->string('jenis-usaha',90);
            $table->string('alamat-usaha',90);
            $table->text('deskripsi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('brosur_digitals');
    }
};
