<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('program_menarik', function (Blueprint $table) {
            $table->id();
            $table->string('user_id',90);
            $table->string('nama',90);
            $table->string('email',90);
            $table->string('no_hp',90);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('program_menariks');
    }
};
