<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_config', function (Blueprint $table) {
            $table->id();
            $table->string('kodewarna',15);
            $table->string('dapil',200);
            $table->text('foto1');
            $table->text('foto2');
            $table->text('logo');
            $table->string('nama_kartu',200);
            $table->string('caleg1',200);
            $table->string('caleg2',200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_configs');
    }
};
