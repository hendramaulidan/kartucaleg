<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Landing</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">

    <style>
        .custom-input-file::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-input-file {
            z-index: 10;
        }

        .custom-input-file::before {
            text-align: center;
            width: 100%;
            height: 100%;
            content: ' ';
            display: inline-block;
            background-color: #ffffff00;
            border: 3px solid #000000;
            border-radius: 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
        }

        .custom-input-file:hover::before {
            background-color: rgba(0, 0, 0, 0.025);
        }

        .custom-input-group {
            position: relative;
        }

        .custom-label {
            z-index: 0;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%)
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <section>
        <div class="container py-2 h-100">

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="card border-0 shadow-lg">
                        <div class="card-header"
                            style="background-color: {{ $config->kodewarna }}; color: white; font-size: 16px">
                            <div class="row justify-content-center">
                                <div class="col-3 text-center">
                                    <img class="mx-auto rounded-circle"
                                        src="{{ asset('/') }}assets/img/{{ $config->logo }}" width="80px"
                                        height="80px" alt="">
                                </div>

                                <div class="col-12 text-center fw-bold fs-6 text-capitalize">
                                    Hai {{ Auth::user()->name }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            @if (Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            @endif

                            @if (Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row d-flex justify-content-center align-items-center h-100">
                                <div class="col-lg-12">

                                    <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src="{{ asset('/') }}assets/img/{{ $config->foto1 }}"
                                                    class="d-block w-100" alt="...">
                                            </div>
                                            <div class="carousel-item">
                                                <img src="{{ asset('/') }}assets/img/{{ $config->foto2 }}"
                                                    class="d-block w-100" alt="...">
                                            </div>
                                        </div>
                                        <button class="carousel-control-prev" type="button"
                                            data-bs-target="#carouselExampleAutoplaying" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button"
                                            data-bs-target="#carouselExampleAutoplaying" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                    <div class="row mt-4 row-gap-4">
                                        <div class="col-12 text-center text-white">
                                            <div class="card card-body border-0"
                                                style="background-image:{{ $config->color_gradient }}">
                                                <h3 class="text-uppercase">
                                                    MANFAAT KARTU {{ $config->nama_kartu }}
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <button data-bs-toggle="modal" data-bs-target="#keagenanPPOB"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Gratis
                                                Keagenan
                                                PPOB</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="keagenanPPOB" tabindex="-1"
                                            aria-labelledby="keagenanPPOBLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="keagenanPPOBLabel">Keagenan
                                                            PPOB</h1>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                            aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.ppob') }}" method="post">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="Nama" class="form-label">Nama</label>
                                                                <input type="text" class="form-control"
                                                                    name="nama" id="Nama"
                                                                    placeholder="Masukkan Nama Anda" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="Email" class="form-label">Email</label>
                                                                <input type="text" class="form-control"
                                                                    name="email" id="Email"
                                                                    placeholder="Masukkan Email Anda" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="NomorHp" class="form-label">Nomor
                                                                    Hp</label>
                                                                <input type="number" class="form-control"
                                                                    name="no_hp" id="NomorHp"
                                                                    placeholder="Masukkan Nomor Hp Anda" />
                                                            </div>
                                                            <hr class="text-danger">
                                                            <p class="text-danger">
                                                                Petugas kami akan menghubungi anda paling lama 7 hari
                                                                kerja untuk pendaftaran akun Keagenan PPOB Anda.
                                                                Pastikan nomor hp Anda tetap aktif.
                                                            </p>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <!-- @if ($existingData || App\Models\UserWebsitePrem::where('user_id', Auth::id())->first())
                                                <button class="btn btn-secondary w-100 text-center"
                                                    style="height: 80px;" disabled>Gratis Pembuatan
                                                    Website</button>
                                            @else
                                            @endif -->
                                            <button data-bs-toggle="modal" data-bs-target="#pembuatanWebsite"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Gratis
                                                Pembuatan
                                                Website</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="pembuatanWebsite" tabindex="-1"
                                            aria-labelledby="pembuatanWebsiteLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="pembuatanWebsiteLabel">
                                                            Pembuatan Website</h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.store') }}" method="POST"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="model-usaha" class="form-label">Nama
                                                                    Usaha</label>
                                                                <input type="text" class="form-control"
                                                                    name="model-usaha" id="model-usaha"
                                                                    placeholder="Nama Usaha / Toko" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="jenis-usaha" class="form-label">Pilih
                                                                    Jenis Usaha</label>
                                                                <select name="jenis-usaha"
                                                                    id="jenis-usaha" class="form-select">
                                                                    <option value="Pilih Jenis Usaha">Pilih Jenis Usaha</option>
                                                                    @foreach ($jenis_usaha as $ju)
                                                                        <option value="{{ $ju->usaha }}">
                                                                            {{ $ju->usaha }}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="alamat-usaha" class="form-label">Alamat
                                                                    Usaha</label>
                                                                <textarea class="form-control" name="alamat-usaha" id="alamat-usaha" cols="10" rows="5"></textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="deskripsi" class="form-label">Deskripsi
                                                                    Usaha</label>
                                                                <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3"></textarea>
                                                            </div>

                                                            <div class="mb-3 domain">
                                                                <label for="domain"
                                                                    class="form-label">Domain</label>
                                                                <input type="text" class="form-control"
                                                                    name="subdomain" id="domain"
                                                                    placeholder="Masukkan Domain / Nama Website Anda" />
                                                                <small class="text-danger"
                                                                    style="font-size:13px">Website Anda akan
                                                                    menggunakan Subdomain dari website utama Kartu
                                                                    Masir, contoh : domain-anda.kartumasir.com .</small>
                                                            </div>

                                                            <!-- <div class="mb-3 domain">
                                                                <label for="domain" class="form-label">Jenis</label>
                                                                <div class="form-check">
                                                                    <input class="form-check-input border border-dark"
                                                                        value="domain utama" type="radio"
                                                                        name="jenis_domain" id="domain1">
                                                                    <label class="form-check-label" for="domain1">
                                                                        Domain Utama
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input border border-dark"
                                                                        value="subdomain" type="radio"
                                                                        name="jenis_domain" id="jenis_do">
                                                                    <label class="form-check-label" for="jenis_do">
                                                                        Subdomain
                                                                    </label>
                                                                </div>
                                                            </div> -->
                                                            <card
                                                                class="card card-body border-0 shadow bg-success text-white text-center">
                                                                <h6>Dapatkan 50 Domain Premium (www.domain-anda.com)
                                                                    tanpa subdomain dengan menjadi koordinator
                                                                    pemenangan {{ $config->sebutan }}</h6>
                                                                <a href="http://" data-bs-toggle="modal" data-bs-target="#pembuatanWebsitePrem" target="_blank" data-bs-dismiss="modal"
                                                                    class="btn btn-dark">Daftar</a>
                                                            </card>
                                                            <p class="text-danger mt-2">
                                                                Petugas kami akan menghubungi anda paling lama 7 hari
                                                                kerja untuk pembuatan website Anda.
                                                                Pastikan nomor hp Anda tetap aktif.
                                                            </p>
                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="pembuatanWebsitePrem" tabindex="-1"
                                            aria-labelledby="pembuatanWebsitePremLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="pembuatanWebsitePremLabel">
                                                            Pembuatan Website Premium</h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.storePrem') }}" method="POST"
                                                        enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="model-usaha" class="form-label">Nama
                                                                    Usaha</label>
                                                                <input type="text" class="form-control"
                                                                    name="model-usaha" id="model-usaha"
                                                                    placeholder="Nama Usaha / Toko" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="jenis-usaha" class="form-label">Pilih
                                                                    Jenis Usaha</label>
                                                                <select name="jenis-usaha"
                                                                    id="jenis-usaha" class="form-select">
                                                                    <option value="Pilih Jenis Usaha">Pilih Jenis Usaha</option>
                                                                    @foreach ($jenis_usaha as $ju)
                                                                        <option value="{{ $ju->usaha }}">
                                                                            {{ $ju->usaha }}</option>
                                                                    @endforeach
                                                                </select>

                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="alamat-usaha" class="form-label">Alamat
                                                                    Usaha</label>
                                                                <textarea class="form-control" name="alamat-usaha" id="alamat-usaha" cols="10" rows="5"></textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="deskripsi" class="form-label">Deskripsi
                                                                    Usaha</label>
                                                                <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3"></textarea>
                                                            </div>

                                                            <div class="mb-3 domain">
                                                                <label for="domain"
                                                                    class="form-label">Domain</label>
                                                                <input type="text" class="form-control"
                                                                    name="domain" id="domain"
                                                                    placeholder="Masukkan Domain / Nama Website Anda" />
                                                                <small class="text-danger"
                                                                    style="font-size:13px">Website Anda akan
                                                                    menggunakan domain sendiri, contoh : domain-anda.com .</small>
                                                            </div>

                                                            <!-- <div class="mb-3 domain">
                                                                <label for="domain" class="form-label">Jenis</label>
                                                                <div class="form-check">
                                                                    <input class="form-check-input border border-dark"
                                                                        value="domain utama" type="radio"
                                                                        name="jenis_domain" id="domain1">
                                                                    <label class="form-check-label" for="domain1">
                                                                        Domain Utama
                                                                    </label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input class="form-check-input border border-dark"
                                                                        value="subdomain" type="radio"
                                                                        name="jenis_domain" id="jenis_do">
                                                                    <label class="form-check-label" for="jenis_do">
                                                                        Subdomain
                                                                    </label>
                                                                </div>
                                                            </div> -->
                                                            <p class="text-danger mt-2">
                                                                Petugas kami akan menghubungi anda paling lama 7 hari
                                                                kerja untuk pembuatan website Anda.
                                                                Pastikan nomor hp Anda tetap aktif.
                                                            </p>
                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <button data-bs-toggle="modal" data-bs-target="#pembuatanBrosur"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Gratis
                                                Pembuatan Brosur
                                                Digital</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="pembuatanBrosur" tabindex="-1"
                                            aria-labelledby="pembuatanBrosurLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="pembuatanBrosurLabel">
                                                            Pembuatan
                                                            Brosur</h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.brosur') }}" method="POST"
                                                        enctype="multipart/form-data">
                                                        @csrf

                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="model-usaha" class="form-label">Nama
                                                                    Usaha</label>
                                                                <input type="text" class="form-control"
                                                                    name="model-usaha" id="model-usaha"
                                                                    placeholder="Nama Usaha / Toko" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="jenis-usaha" class="form-label">Pilih
                                                                    Jenis
                                                                    Usaha</label>
                                                                <select name="jenis-usaha"
                                                                    id="jenis-usaha"class="form-select">
                                                                    <option value="Pilih Jenis Usaha">Pilih Jenis Usaha</option>
                                                                    @foreach ($jenis_usaha as $ju)
                                                                        <option value="{{ $ju->usaha }}">
                                                                            {{ $ju->usaha }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="alamat-usaha" class="form-label">Alamat
                                                                    Usaha</label>
                                                                <textarea class="form-control" name="alamat-usaha" id="alamat-usaha" cols="10" rows="5"></textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="deskripsi" class="form-label">Deskripsi
                                                                    Usaha</label>
                                                                <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3"></textarea>
                                                            </div>
                                                            <div class="mb-3">
                                                                <div class="mb-3">
                                                                    <label for="formFile" class="form-label">Upload
                                                                        Foto Usaha/Produk</label>
                                                                    <input class="form-control" type="file"
                                                                        id="formFile"name="foto_usaha[]"multiple>
                                                                </div>
                                                            </div>
                                                            <p class="text-danger">
                                                                Petugas kami akan menghubungi anda paling lama 7 hari
                                                                kerja untuk pembuatan Brosur Digital Anda. Pastikan
                                                                nomor hp Anda tetap aktif.
                                                            </p>

                                                        </div>


                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <button data-bs-toggle="modal" data-bs-target="#merchandiDapil"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Gratis/Diskon
                                                Merchant di
                                                Dapil</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="merchandiDapil" tabindex="-1"
                                            aria-labelledby="merchandiDapilLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="merchandiDapilLabel">Merchant
                                                            di
                                                            Dapil</h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.merchan') }}" method="post">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="Dapil" class="form-label">Dapil</label>
                                                                <select name="dapil"
                                                                    id="dapil"class="form-select" onchange="getToko()">
                                                                    <option value="Pilih Kota"selected disabled>Pilih Kota</option>
                                                                    @foreach ($dapil_kota as $dk)
                                                                        <option value="{{ $dk->id }}|{{ $dk->kota }}">
                                                                            {{ $dk->kota }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="Jenis" class="form-label">Jenis
                                                                    Toko</label>
                                                                <select name="jenis_toko"
                                                                    id="jentok" class="form-select" onchange="getToko()">
                                                                    <option value="Pilih Jenis Toko"selected disabled>Pilih Jenis Toko</option>
                                                                    @foreach ($jenis_toko as $jt)
                                                                        <option value="{{ $jt->id }}|{{ $jt->jenis }}">
                                                                            {{ $jt->jenis }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="Jenis" class="form-label">Toko</label>
                                                                <select name="toko"
                                                                    id="listToko" class="form-select">
                                                                    <option value="Pilih Toko"selected disabled>Pilih
                                                                        Toko</option>
                                                                    {{-- @foreach ($toko as $tk)
                                                                        <option value="{{ $tk->id }}|{{ $tk->toko }}">
                                                                            {{ $tk->toko }}</option>
                                                                    @endforeach --}}
                                                                </select>
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="Nama" class="form-label">Nama</label>
                                                                <input type="text" class="form-control"
                                                                    name="nama" id="Nama"
                                                                    placeholder="Masukkan Nama Anda" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="nomor_kartu" class="form-label">Nomor
                                                                    Kartu</label>
                                                                <input type="text" class="form-control"
                                                                    name="nomor_kartu" id="nomor_kartu"
                                                                    placeholder="Masukkan Nomor Kartu Anda" />
                                                            </div>



                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <button data-bs-toggle="modal" data-bs-target="#webinar"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Gratis
                                                Webinar dan Workshop Kewirausahaan</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="webinar" tabindex="-1"
                                            aria-labelledby="webinarLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="webinarLabel">Webinar dan
                                                            Workshop Kewirausahaan</h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.webinar') }}" method="post">
                                                        @csrf
                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="Dapil" class="form-label">Dapil</label>
                                                                <select name="dapil"
                                                                    id=""class="form-select">
                                                                    <option value="Pilih Kota"selected disabled>Pilih
                                                                        Kota</option>
                                                                    @foreach ($dapil_kota as $dk)
                                                                        <option value="{{ $dk->kota }}">
                                                                            {{ $dk->kota }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="Jenis" class="form-label">Jenis
                                                                    Workshop</label>
                                                                <select name="workshop"
                                                                    id="jewor"class="form-select" onchange="getStatus()">
                                                                    <option value="Pilih Workshop"selected disabled>Pilih
                                                                        Jenis Workshop</option>
                                                                    <option value="1">Workshop Online</option>
                                                                    <option value="0">Workshop Offline
                                                                    </option>

                                                                </select>
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="Jenis" class="form-label">Tema
                                                                    Workshop</label>
                                                                <select name="tema"
                                                                    id="pilihTema" class="form-select">
                                                                    <option value="Pilih Tema"selected disabled>Pilih
                                                                        Tema</option>
                                                                </select>
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="Nama" class="form-label">Nama</label>
                                                                <input type="text" class="form-control"
                                                                    name="nama" id="Nama"
                                                                    placeholder="Masukkan Nama Anda" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="nomor_kartu" class="form-label">Nomor
                                                                    Kartu</label>
                                                                <input type="text" class="form-control"
                                                                    name="nomor_kartu" id="nomor_kartu"
                                                                    placeholder="Masukkan Nomor Kartu Anda" />
                                                            </div>


                                                        </div>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <button data-bs-toggle="modal" data-bs-target="#bantuan_caleg"
                                                class="btn w-100 text-center"
                                                style="height: 80px; background-color: {{ $config->kodewarna }}; color: white">Program
                                                Bantuan Langsung Caleg</button>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade" id="bantuan_caleg" tabindex="-1"
                                            aria-labelledby="webinarLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h1 class="modal-title fs-5" id="webinarLabel">Program Bantuan
                                                            Langsung Caleg
                                                        </h1>
                                                        <button type="button" class="btn-close"
                                                            data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <form action="{{ route('claim.programMenarik') }}"
                                                    method="post">
                                                        @csrf

                                                        <div class="modal-body">
                                                            <div class="mb-3">
                                                                <label for="Dapil" class="form-label">Dapil</label>
                                                                <select name="dapil"
                                                                    id="" class="form-select">
                                                                    <option value="Pilih Kota"selected disabled>Pilih
                                                                        Kota</option>
                                                                    @foreach ($dapil_kota as $dk)
                                                                        <option value="{{ $dk->kota }}">
                                                                            {{ $dk->kota }}</option>
                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="Jenis" class="form-label">Jenis
                                                                    Bantuan</label>
                                                                <select name="jenis_bantuan"
                                                                    id=""class="form-select">
                                                                    <?php $bantuan = App\Models\Bantuan::get(); ?>

                                                                    <option value="Pilih Kota"selected disabled>Pilih
                                                                        Jenis Bantuan</option>
                                                                    @foreach ($bantuan as $dk)
                                                                    <option value="{{$dk->bantuan}}">{{$dk->bantuan}}</option>
                                                                    @endforeach
                                                                   

                                                                </select>
                                                            </div>

                                                            <div class="mb-3">
                                                                <label for="Nama" class="form-label">Nama</label>
                                                                <input type="text" class="form-control"
                                                                    name="nama" id="Nama"
                                                                    placeholder="Masukkan Nama Anda" />
                                                            </div>
                                                            <div class="mb-3">
                                                                <label for="nomor_kartu" class="form-label">Nomor
                                                                    Kartu</label>
                                                                <input type="text" class="form-control"
                                                                    name="nomor_kartu" id="nomor_kartu"
                                                                    placeholder="Masukkan Nomor Kartu Anda" />
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button style="background-color: {{ $config->kodewarna }}"
                                                                type="submit" class="btn text-white">Kirim</button>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>



                                </div>


                            </div>

                        </div>

                    </div>
                    <div class="card card-body border-0 mt-2 shadow-lg">
                        <div class="row mt-2 row-gap-4">
                            <div class="col-12">

                                <div class="row row-gap-4">
                                    <div class="col-12">
                                        <h5 class="text-center text-danger fw-bold">
                                            Laporan Saksi & Laporan Kecurangan Pemilu Legislatif 2024
                                        </h5>
                                        <hr>
                                    </div>
                                    <div class="col-6">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-dark" type="button" data-bs-toggle="modal"
                                                data-bs-target="#laporSaksi">
                                                Input C1 TPS</button>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="d-grid gap-2">
                                            <button class="btn btn-dark" type="button" data-bs-toggle="modal"
                                                data-bs-target="#laporKecurangan">Kecurangan Pemilu</button>
                                        </div>
                                    </div>


                                    <!-- Modal -->
                                    <div class="modal fade" id="laporSaksi" tabindex="-1"
                                        aria-labelledby="laporSaksiLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title fs-5" id="laporSaksiLabel">
                                                        Input C1 TPS</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <form action="#" method="post">

                                                    <div class="modal-body">
                                                        <div class="mb-3">
                                                            <label for="Nama" class="form-label">Nama</label>
                                                            <input type="text" class="form-control" name="nama"
                                                                id="Nama" placeholder="Masukkan Nama Anda" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="Email" class="form-label">Email</label>
                                                            <input type="text" class="form-control" name="email"
                                                                id="Email" placeholder="Masukkan Email Anda" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <div>
                                                                <label for="kecurangan" class="form-label">Foto
                                                                    C1</label>
                                                                <input class="form-control form-control-lg"
                                                                    id="kecurangan" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">Close</button>
                                                        <button style="background-color: {{ $config->kodewarna }}"
                                                            type="button" class="btn text-white">Kirim</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="laporKecurangan" tabindex="-1"
                                        aria-labelledby="laporKecuranganLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title fs-5" id="laporKecuranganLabel">Lapor
                                                        Kecurangan
                                                        Pemilu</h1>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <form action="#" method="post">

                                                    <div class="modal-body">
                                                        <div class="mb-3">
                                                            <label for="Nama" class="form-label">Nama</label>
                                                            <input type="text" class="form-control" name="nama"
                                                                id="Nama" placeholder="Masukkan Nama Anda" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="Email" class="form-label">Email</label>
                                                            <input type="text" class="form-control" name="email"
                                                                id="Email" placeholder="Masukkan Email Anda" />
                                                        </div>
                                                        <div class="mb-3">
                                                            <label for="deskripsi"
                                                                class="form-label">Deskripsi</label>
                                                            <textarea class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan deskripsi Kecurangan"
                                                                cols="30" rows="10"></textarea>
                                                        </div>
                                                        <div class="mb-3">
                                                            <div>
                                                                <label for="kecurangan" class="form-label">Foto/Video
                                                                    Bukti
                                                                    Kecurangan</label>
                                                                <input class="form-control form-control-lg"
                                                                    id="kecurangan" type="file">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">Close</button>
                                                        <button style="background-color: {{ $config->kodewarna }}"
                                                            type="button" class="btn text-white">Kirim</button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="col-12">
                                <form action="{{ route('logout') }}" method="POST">
                                    @csrf
                                    <div class="d-grid gap-2">
                                        <button type="submit" class="btn w-100 text-center btn-danger"
                                            onclick="return confirm('anda yakin ingin logout?')">Logout</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>


        <!-- Modal notif -->

        <div class="modal fade" id="modalDeskripsi" tabindex="-1" aria-labelledby="modalDeskripsiLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="modalDeskripsiLabel">Pembuatan Website</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <form action="{{ route('claim.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-body">



                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"
                                    data-bs-dismiss="modal">Close</button>
                                <button style="background-color: {{ $config->kodewarna }}" type="submit"
                                    class="btn text-white">Kirim</button>
                            </div>

                    </form>
                </div>
            </div>
        </div>





    </section>


    <div class="modal fade" id="modalSuccessCreate" tabindex="-1" aria-labelledby="modalSuccessCreateLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <!-- <div class="modal-header">
                    <h1 class="modal-title fs-5" id="modalSuccessCreateLabel"></h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div> -->
                <div class="modal-body">
                    @if (Session::has('success_create'))
                        <div class="alert alert-success">
                            {{ Session::get('success_create') }}
                        </div>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
        integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script>

    <script>
        var modalSuccessCreate = new bootstrap.Modal(document.getElementById("modalSuccessCreate"), {});
        @if (Session::has('success_create'))
            document.onreadystatechange = function() {

                modalSuccessCreate.show();
            };
        @endif
    </script>

    <script>
        function getToko() {
            let idJt = $('#jentok').val()
            idJt = idJt.split("|")[0];
            let idDapil = $('#dapil').val()
            idDapil = idDapil.split("|")[0];
            // console.log(idJt);
            $.ajax({
                url: `{{ url('') }}/claim/getJenis/`,
                method: 'GET',
                dataType: 'json',
                data: {idJt, idDapil},
                success: function(response) {
                    var $select = $('#listToko');
                    $select.html("");
                    $.each(response, function(index, item) {


                        var $option = $('<option>').val(`${item.id}|${item.toko}`).text(
                            item.toko);
                        $select.append($option);
                        // console.log(item);


                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }
    </script>
    <script>
        function getStatus() {
            let idJw = $('#jewor').val()
            // console.log(idJt);
            $.ajax({
                url: `{{ url('') }}/claim/getTema/`,
                method: 'GET',
                dataType: 'json',
                data: {idJw},
                success: function(response) {
                    var $select = $('#pilihTema');
                    $select.html("");
                    $.each(response, function(index, item) {


                        var $option = $('<option>').val(`${item.tema}`).text(
                            item.tema);
                        $select.append($option);
                        // console.log(item);


                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }
    </script>
</body>

</html>
