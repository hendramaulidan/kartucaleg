@extends('layouts.main')
@section('content')
    <style>
        .card {
            position: relative;
        }

        .card .icon {
            position: absolute;
            right: 20px;
            top: 50%;
            transform: translateY(-50%)
        }

        .card .icon path {
            opacity: 0.5;
        }
    </style>

    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">
                <div class="row">
                    <div class="col-lg-4 mb-3">
                        <h1>DASHBOARD {{ $config->nama_pasangan }}</h1>
                        <h3>Pemilu Legislatif 2024</h3>
                    </div>
                    <div class="col-lg-2">
                        <div class="card border-0 shadow-lg bg-dark text-light p-0">
                            <div class="card-header p-2 text-center">
                                <b>
                                    Jumlah Kartu Dibuat
                                </b>
                            </div>
                            <div class="card-body text-center p-2">
                                <h5>
                                    {{ intval($config->jumlah_kartu) }}
                                </h5>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-2">

                        <div class="card border-0 shadow-lg bg-primary text-light p-0">
                            <div class="card-header p-2 text-center">
                                <b>
                                    Jumlah kartu Terdaftar
                                </b>
                            </div>
                            <div class="card-body text-center p-2">
                                <h5>
                                    {{ $jumlah_user }}
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2">

                        <div class="card border-0 shadow-lg bg-info text-light p-0">
                            <div class="card-header p-2 text-center">
                                <b>
                                    Jumlah Kartu Digunakan
                                </b>
                                <?php
                                
                                $userIds = [];
                                
                                $tables = ['brosur_digital', 'merchan', 'ppob', 'program_menarik', 'user_website', 'user_website_prem', 'webinar'];
                                
                                foreach ($tables as $table) {
                                    $userIdsFromTable = DB::table($table)
                                        ->pluck('user_id')
                                        ->toArray();
                                    $userIds = array_merge($userIds, $userIdsFromTable);
                                }
                                
                                $uniqueUserCount = count(array_unique($userIds));
                                ?>
                            </div>
                            <div class="card-body text-center p-2">
                                <h5>
                                    {{ $uniqueUserCount }}
                                </h5>
                            </div>
                        </div>
                    </div>


                <div class="col-lg-2">


                    <div class="card border-0 shadow-lg bg-danger text-light p-0">
                        <div class="card-header p-2 text-center">
                            <b>
                                Jumlah Belum Terdaftar
                            </b>
                        </div>
                        <div class="card-body text-center p-2">
                            <h5>
                                {{ intval($config->jumlah_kartu) - intval($jumlah_user) }}
                            </h5>
                        </div>
                    </div>


                </div>

            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-facebook content-center text-center text-white p-2">
                            <h3>
                                PPOB
                            </h3>
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ $user_ppob }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_ppob_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_ppob_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-info content-center text-center text-white p-2">
                            <h3>
                                Website Reguler
                            </h3>
                        </div>
                        <div class="card-body row text-center">

                            <div class="col">
                                <div class="text-value-xl">{{ $user_website }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_website_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_website_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-primary content-center text-center text-white p-2">
                            <h3>
                                Website Premium
                            </h3>
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ $user_website_prem }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_website_prem_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_website_prem_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-warning content-center text-center text-white p-2">
                            <h3>
                                Brosur Digital
                            </h3>
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ $user_brosur }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_brosur_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_brosur_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-success content-center text-center text-white p-2">
                            <h3>
                                Webinar
                            </h3>
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ $user_webinar }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_webinar_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_webinar_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="card">
                        <div class="card-header bg-danger content-center text-center text-white p-2">
                            <h3>
                                Merchant Dapil
                            </h3>
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ $user_merchant }}</div>
                                <div class="text-uppercase text-muted small">Masuk</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_merchant_proses }}</div>
                                <div class="text-uppercase text-muted small">Di Proses</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ $user_merchant_selesai }}</div>
                                <div class="text-uppercase text-muted small">Selesai</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-12">

                    <div class="card border-0 shadow">
                        <div class="card-header text-center text-uppercase">
                            <h3>
                                Bantuan Langsung Caleg
                            </h3>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>
                                            no
                                        </th>
                                        <th>
                                            Bantuan
                                        </th>

                                        <th>
                                            Masuk
                                        </th>
                                        <th>
                                            Di Proses
                                        </th>
                                        <th>
                                            Selesai
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $i = 1; ?>
                                    @foreach ($bantuan as $bantu)
                                        <tr>
                                            <td>
                                                {{ $i++ }}
                                            </td>
                                            <td>
                                                {{ $bantu->bantuan }}
                                            </td>
                                            <?php $jmlUserMasuk = App\Models\ProgramMenarik::where('jenis_bantuan', $bantu->bantuan)
                                                ->where('status', 0)
                                                ->count(); ?>
                                            <td>
                                                {{ $jmlUserMasuk }}
                                            </td>
                                            <?php $jmlUserProses = App\Models\ProgramMenarik::where('jenis_bantuan', $bantu->bantuan)
                                                ->where('status', 1)
                                                ->count(); ?>
                                            <td>
                                                {{ $jmlUserProses }}
                                            </td>
                                            <?php $jmlUserSelesai = App\Models\ProgramMenarik::where('jenis_bantuan', $bantu->bantuan)
                                                ->where('status', 2)
                                                ->count(); ?>
                                            <td>
                                                {{ $jmlUserSelesai }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>


            </div>
            <!-- /.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3>
                                Data Pengguna
                            </h3>
                            <a
                                href="{{ route('admin.unduhDatabase') }}"class="btn btn-dark float-right"style="margin-top:-40px">&nbsp;&nbsp;&nbsp;Unduh
                                Database&nbsp;&nbsp;&nbsp;</a>
                        </div>
                        <div class="card-body">
                            <livewire:list-user-data />
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
        </div>
    </main>
@endsection
