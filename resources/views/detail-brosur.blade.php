@extends('layouts.main')
@section('content')
    <main class="c-main">
        <div class="container-fluid  text-capitalize">
            <div class="fade-in">
                <h1 class="mb-3 text-capitalize">Detail Brosur Digital {{ $user->user->name }}</h1>
                <!-- /.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h2>
                                    @if ($user->status == 0)
                                        <div class="badge badge-secondary float-right">
                                            &nbsp;&nbsp;Menunggu&nbsp;&nbsp;
                                        </div>
                                    @elseif ($user->status == 1)
                                        <div class="badge badge-primary float-right">
                                            &nbsp;&nbsp;Sedang Di Proses&nbsp;&nbsp;
                                        </div>
                                    @else
                                        <div class="badge badge-success float-right">
                                            &nbsp;&nbsp;Selesai&nbsp;&nbsp;
                                        </div>
                                    @endif
                                </h2>

                            </div>
                            <div class="card-body">

                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif

                                @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif



                                <div class="form-group">
                                    <label for="nama">Foto Usaha</label>
                                    <?php $foto = explode('|', $user->foto_usaha); ?>
                                    <div class="row">

                                        @foreach ($foto as $fot)
                                            <div class="col-lg">
                                                <img src="{{asset('/storage').'/'.$fot}}" alt="" srcset=""class="img-thumbnail img-fluid"style="height:300px">
                                            </div>
                                        @endforeach
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama"
                                        value="{{ $user->user->name }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="no_hp">No Whatsapp</label>
                                    <?php $userp = App\Models\UserPublic::where('user_id', $user->user->id)->first(); ?>
                                    <input type="no_hp" class="form-control" id="no_hp" value="{{ $userp->no_hp }}"
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email"
                                        value="{{ $user->user->email }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="model_usaha">Nama Usaha</label>
                                    <input type="text" class="form-control" id="model_usaha"
                                        value="{{ $user->model_usaha }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="jenis_usaha">Jenis Usaha</label>
                                    <input type="text" class="form-control" id="jenis_usaha"
                                        value="{{ $user->jenis_usaha }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label for="nomor-seri">Nomor Seri</label>
                                    <input type="text" class="form-control" id="nomor-seri"
                                        value="{{ $user->user->kode_kartu }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi Usaha</label>
                                    <textarea name="" class="form-control"id="deskripsi" cols="30" rows="10"readonly>{{ $user->deskripsi }}</textarea>
                                </div>

                                @if ($user->status == 0)
                                    <form action="{{ route('admin.updateBrosur', $user->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="status"value="1">
                                        <button type="submit"class="btn btn-primary btn-lg btn-block">Proses
                                            Brosur</button>
                                    </form>
                                @elseif ($user->status == 1)
                                    <form action="{{ route('admin.updateBrosur', $user->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="status"value="2">
                                        <button type="submit"class="btn btn-dark btn-lg btn-block">Selesai</button>
                                    </form>
                                @else
                                @endif



                            </div>

                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
            </div>
        </div>
    </main>
@endsection
