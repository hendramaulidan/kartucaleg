@extends('layouts.main')
@section('content')
    <main class="c-main">
        <div class="container-fluid  text-capitalize">
            <div class="fade-in">
                <h1 class="mb-3 text-capitalize">Detail Dokumentasi Publikasi</h1>
                <!-- /.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body">

                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif

                                @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif



                                <div class="form-group">
                                    <label for="nama">Foto Dokumentasi</label>
                                    <?php $foto = explode('|', $user->foto); ?>
                                    <div class="row justify-content-center">

                                        @foreach ($foto as $fot)
                                            <div class="col-lg">
                                                <img src="{{ asset('/storage') . '/' . $fot }}" alt=""
                                                    srcset=""class="img-thumbnail img-fluid"style="height:300px">
                                            </div>
                                        @endforeach
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label for="deskripsi">Deskripsi</label>
                                    <textarea class="form-control" id="deskripsi" readonly cols="30" rows="10">{{ $user->deskripsi }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" readonly cols="30" rows="10">{{ $user->alamat }}</textarea>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
            </div>
        </div>
    </main>
@endsection
