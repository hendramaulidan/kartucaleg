@extends('layouts.main')
@section('content')
    <main class="c-main">
        <div class="container-fluid  text-capitalize">
            <div class="fade-in">
                <h1 class="mb-3 text-capitalize">Detail {{ $user->name }}</h1>
                <!-- /.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">

                                    <img src="{{ asset('storage') . '/' . $user->userPublic->foto_ktp }}" alt="Foto KTP"
                                        class="img-thumbnail"style="height:250px">
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input type="text" class="form-control" id="nama" value="{{ $user->name }}"
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" id="email" value="{{ $user->email }}"
                                        readonly>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea class="form-control" id="alamat" rows="3" readonly>{{ $user->userPublic->alamat }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="nomor-seri">Nomor Seri</label>
                                    <input type="text" class="form-control" id="nomor-seri"
                                        value="{{ $user->kode_kartu }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="TPS">Nomor TPS</label>
                                    <input type="text" class="form-control" id="TPS"
                                        value="{{ $user->userPublic->tps }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="nomor-hp">Nomor HP</label>
                                    <input type="text" class="form-control" id="nomor-hp" value="{{ $user->userPublic->no_hp }}"
                                        readonly>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
            </div>
        </div>
    </main>
@endsection
