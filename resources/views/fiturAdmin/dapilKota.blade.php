@extends('layouts.main')
@section('content')
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <h1 class="mb-3">List Dapil Kota</h1>

                <!-- /.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif

                                @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <button class="btn btn-success float-right"data-toggle="modal"
                                    data-target="#modalTambah"type="button">Tambah
                                    Kota</button>
                            </div>
                            <div class="card-body">
                                <livewire:list-dapil />
                            </div>

                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="modalTambahLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="ModalTambahLabel">Tambah Kota</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.tambahDapilKota') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-outline mb-3 form-group">
                            <h5> Pilih Provinsi : <br></h5>
                            <select class="form-control" id="form6Example1q" name="provinsi" onchange="getKota(this)">
                                <option value="" selected disabled>PROVINSI</option>
                                @foreach ($provinsi as $prov)
                                    <option value="{{ $prov['id'] }}|{{ $prov['name'] }}">
                                        {{ $prov['name'] }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-outline mb-3">
                            <h5> Pilih Kota : <br></h5>
                            <select class="form-control" id="kabKota" name="kota">

                                <option value="" selected disabled>KOTA</option>

                            </select>
                        </div>

                        <div class="form-outline mb-3">
                            <h5> Password <br></h5>
                            <input type="password" name="password" class="form-control"placeholder="password">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
