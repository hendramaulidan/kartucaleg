@extends('layouts.main')
@section('content')
    <main class="c-main">
        <div class="container-fluid">
            <div class="fade-in">

                <h1 class="mb-3">Jenis Usaha</h1>

                <!-- /.row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">

                            @if (Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                            @endif

                            @if (Session::has('error'))
                                <div class="alert alert-danger">
                                    {{ Session::get('error') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif


                                <button class="btn btn-success float-right"data-toggle="modal"
                                    data-target="#modalTambah"type="button">Tambah
                                    Jenis Usaha</button>
                            </div>
                            <div class="card-body">



                                <livewire:list-jenis-usaha/>
                            </div>

                        </div>
                    </div>
                    <!-- /.col-->
                </div>
                <!-- /.row-->
            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="modalTambahLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="ModalTambahLabel">Tambah Jenis Usaha</h1>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.tambahJenisUsaha') }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-outline mb-3 form-group">
                        

                          
                            <h5>Masukan Jenis Usaha: <br></h5>
                            <input type="text"name="jenis_usaha"class="form-control"placeholder="Masukan Jenis Usaha">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
