<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Whatsapp Broadcast</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('style.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    
</head>
<style>
    .btn-color {
        background-color: #0e1c36;
        color: #fff;

    }

    .profile-image-pic {
        height: 200px;
        width: 200px;
        object-fit: cover;
    }



    .cardbody-color {
        background-color: #ebf2fa;
    }

    a {
        text-decoration: none;
    }

   
</style>

<body style="background-image:{{ $config->color_gradient }}">

    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <h2 class="text-center text-white mt-5">{{ ucwords(strtolower($config->nama_pasangan)) }}</h2>
                <h6 class="text-center text-white mt-1">Login Whatsapp Broadcast</h6>
                <div class="card border-0 shadow-lg"style="background-color:{{ $config->kodewarna }}">

                    <form class="card-body p-lg-5 rounded-3">

                        <div class="text-center">
                            <img src="{{ asset('assets/img/logo_nav.png') }}" class="img-fluid my-3" width="100px"
                                alt="profile">
                        </div>

                        <div class="mb-3">
                            <input type="email" class="form-control" id="Username" aria-describedby="emailHelp"
                                placeholder="Masukan Email Anda">
                        </div>
                        <div class="mb-3">
                            <input type="password" class="form-control" id="password"
                                placeholder="Masukan Password Anda">
                        </div>
                        <div class="text-center"><button type="submit"
                                class="btn btn-color px-5 mb-5 w-100">Login</button></div>

                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header ftco-degree-bg">
                    <!-- <button type="button" class="close d-flex align-items-center justify-content-center"
                        data-dismiss="modal" aria-label="Close"> -->
                        <!-- <span aria-hidden="true" class="ion-ios-close"></span> -->
                    </button>
                </div>
                <div class="modal-body pt-md-0 pb-md-5 text-center">
                    <h2>Whatsapp Broadcast</h2>
                    <div class="icon d-flex align-items-center justify-content-center">
                        <img src="{{ asset('assets/img/logo_nav.png') }}" alt="" class="img-fluid"style="width:100px;height:auto">
                    </div>
                    <h4 class="mb-2"style="margin-top:-47px">System Kartu Caleg harus Aktif <br> sebelum <b style="color:#178646"> Whatsapp Broadcast</b> di aktifkan </h4>
                    <h3>{{ ucwords(strtolower($config->nama_pasangan)) }}</h3>

                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="
        crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>

    <script>
        window.onload = () => {
            $('#staticBackdrop').modal('show');
        }

        (function($) {

            "use strict";

            $('[data-toggle="tooltip"]').tooltip()

            // $('#exampleModalCenter').modal('show')

            var fullHeight = function() {

                $('.js-fullheight').css('height', $(window).height());
                $(window).resize(function() {
                    $('.js-fullheight').css('height', $(window).height());
                });

            };
            fullHeight();

        })(jQuery);
    </script>
</body>

</html>
