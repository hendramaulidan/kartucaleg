@extends('templates.main-landing')
@section('body')

<section class="hero-section d-flex justify-content-center align-items-center" id="section_1"
    style="background-image    : {{$config->color_gradient}};">
    <div class="container" style="padding-top:0px;">
        <!-- <div class="row">
            <div class="col-lg-8 col-12 mx-auto">
                <h2 class="text-white text-center" style="margin-top:-50px;">{{$config->sebutan}}</h2>
                <p class="text-white text-center fw-bold" style="font-size:17px;margin-top:-5px;">
                    {{$config->dapil}}
                </p>
                <p class="text-white text-center fw-bold" style="font-size:15px;margin-top:-10px;">{{$config->daerah}}
                </p>

            </div>

        </div> -->
    </div>
</section>

<section class="featured-section" style="background-color:{{$config->kodewarna}};margin-top:-90px">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-12 desktop">
                <div class="card card-body border-0 rounded-3 p-0 shadow-lg mb-4  hover-card">
                    <div id="carouselExampleAutoplaying" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class=" carousel-item active">
                                <img src="{{asset('/')}}assets/img/{{$config->foto1}}" class="w-100 rounded-3"
                                    style="object-fit:cover" alt="...">
                            </div>
                            <div class="  carousel-item">
                                <img src="{{asset('/')}}assets/img/{{$config->foto2}}" class="w-100 rounded-3"
                                    style="object-fit:cover" alt="...">
                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleAutoplaying"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleAutoplaying"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>




                </div>
            </div>


            <div class="col-lg- col-12  mobile">
                <div class="shadow-lg">
                    <div class="card card-body border-0 rounded-3 mb-4 p-0">
                        <div id="carouselMobile" class="carousel slide rounded-3  mobile" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                <div class=" carousel-item active">
                                    <img src="{{asset('/')}}assets/img/{{$config->foto1}}" class="w-100 rounded-3 "
                                        style="object-fit:cover" alt="...">
                                </div>
                                <div class="  carousel-item">
                                    <img src="{{asset('/')}}assets/img/{{$config->foto2}}" class="w-100 rounded-3 "
                                        style="object-fit:cover" alt="...">
                                </div>
                            </div>
                            <button class="carousel-control-prev" type="button" data-bs-target="#carouselMobile"
                                data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button" data-bs-target="#carouselMobile"
                                data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-12 mb-4">
                <div class="card  rounded-3 shadow-lg hover-card">


                    <div class="card-body">
                        <div class="p-1">
                            <h3 class="card-title fw-bold text-dark">
                                Login
                            </h3>
                            <p class="text-dark" style="font-size:15px">
                                Mohon isi data berikut dengan benar.
                            </p>

                        </div>
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            @if(session()->has('errorLogin'))
                            <div class="alert alert-danger" role="alert">
                                <strong> {{ session()->get('errorLogin') }}</strong>
                            </div>
                            @endif

                            <div class="form-group mb-3">
                                <label for="id-ktp">Nomor Seri Kartu {{$config->nama_kartu}}</label>
                                <input class="form-control" type="text" name="login" value="{{ old('login') }}"
                                    id="id-ktp" placeholder="Ketik Disini...">

                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control" type="password" name="password" required
                                    autocomplete="current-password" id="password" placeholder="Ketik Disini...">
                            </div>


                            <button type="submit" class="btn"
                                style="background-color: {{$config->kodewarna}}; color: white">Login</button>
                            <a href="{{ url('daftar') }}" class="btn btn-secondary">Registrasi Kartu</a>
                            @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="mt-5">Lupa Password?</a>
                            @endif
                        </form>
                    </div>
                </div>
            </div>




        </div>
    </div>
</section>


<section class="explore-section" id="section_2">
    <div class="container mt-1">
        <div class="card card-body border-0">
            <div class="row text-center g-2">
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Unik-and-different.png"
                            class="img-fluid">
                        <span style="font-size:13px" class="fw-bold">
                            Profil
                        </span>
                    </div>
                </div>
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Fungsi-4.png"
                            class="img-fluid"
                            >
                        <span style="font-size:13px" class="fw-bold">
                            Visi
                        </span>
                    </div>
                </div>
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Swara-2.png"
                            class="img-fluid"
                            >
                        <span style="font-size:13px" class="fw-bold">
                            Misi
                        </span>
                    </div>
                </div>
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Secure.png"
                            class="img-fluid"
                            >
                        <span style="font-size:11px" class="fw-bold">
                            Program
                        </span>
                    </div>
                </div>

                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Fungsi-1.png"
                            class="img-fluid"
                            >   
                        <span style="font-size:11px" class="fw-bold">
                            Agenda
                        </span>
                    </div>
                </div>
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/Crash.png"
                            class="img-fluid"
                            >
                        <span style="font-size:13px" class="fw-bold">
                            Posko
                        </span>
                    </div>
                </div>

                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/4-1.png"
                            class="img-fluid"
                            >
                        <span style="font-size:13px" class="fw-bold">
                            Donasi
                        </span>
                    </div>
                </div>
                <div class="col-lg col-sm-3 col-3">
                    <div class="card card-body border-0 mt-2 text-white"
                        style="background-color: {{$config->kodewarna}}">
                        <img src="https://kartuprabowomania.com/wp-content/uploads/2023/08/7.png"
                            class="img-fluid"
                            >
                        <span style="font-size:13px" class="fw-bold">
                            Kontak
                        </span>
                    </div>
                </div>

            </div>
        </div>
        <!-- <div class="row mt-3">
            <div class="col-12 text-center">
                <h2>Manfaat</h2>
                <h2> Kartu {{$config->nama_kartu}}</h2>
            </div>

        </div> -->
    </div>

    <div class="container mt-3">
        <div class="row">

            <div class="col-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="design-tab-pane" role="tabpanel"
                        aria-labelledby="design-tab" tabindex="0">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>

                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-2.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">
                                        <div>
                                            <h5 class="mb-2">PPOB Gratis</h5>

                                            <p class="mb-0">Pemilik Kartu {{$config->nama_kartu}} dapat membuka usaha
                                                keagenan PPOB dengan
                                                gratis. 
                                            </p>
                                        </div>
                                        
                                    </a>
                                </center>
                                    <a href="https://midtrans.com/id/blog/ppob-adalah" target="_blank"class="btn btn-success mt-1" >Apa itu PPOB?</a>
                                <center>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/6-1.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">
                                        <div class="text-center">

                                        </div>
                                        <div>
                                            <h5 class="mb-2">Web Landing</h5>

                                            <p class="mb-0">Pemilik Kartu {{$config->nama_kartu}} yang punya usaha akan
                                                dibuatkan website
                                                gratis.</p>
                                        </div>




                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-4.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">
                                        <div class="text-center">

                                        </div>

                                        <div>
                                            <h5 class="mb-2">Brosur Digital</h5>

                                            <p class="mb-0">Pemilik Kartu {{$config->nama_kartu}} yang ingin dibuatkan
                                                poster atau brosur
                                                produk akan dibuatkan gratis.</p>
                                        </div>



                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-5.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">
                                        <div class="text-center">

                                        </div>

                                        <div>
                                            <h5 class="mb-2">Pelatihan</h5>

                                            <p class="mb-0">Pemilik Kartu {{$config->nama_kartu}} bisa mengikuti
                                                pelatihan kewirausahaan
                                                online/offline.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>

                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-6.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">

                                        <div>
                                            <h5 class="mb-2">Diskon Merchant</h5>

                                            <p class="mb-0">Pemilik Kartu {{$config->nama_kartu}} akan menikmati diskon
                                                khusus untuk
                                                belanja sembako di Dapil Anda.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-7.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">

                                        <div>
                                            <h5 class="mb-2">Interaksi Kandidat</h5>

                                            <p class="mb-0">Pemilik Kartu & kandidat bisa berkomunikasi langsung untuk
                                                menyerap aspirasi.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/Fungsi-8.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">

                                        <div>
                                            <h5 class="mb-2">Program Bantuan</h5>

                                            <p class="mb-0">Caleg dapat menyalurkan bantuan pada pemilik kartu sekaligus
                                                sosialisasi program</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-12 mb-4">
                                <div class="custom-block bg-white shadow-lg">
                                    <center>
                                        <img src="https://kartucaleg.com/wp-content/uploads/2023/08/6.png"
                                            class="custom-block-image img-fluid"
                                            style="height:150px;width:auto;margin-top:-10px" alt="">
                                    </center>
                                    <a href="#" class="mt-2 text-center">

                                        <div>
                                            <h5 class="mb-2">Mobilisasi Relawan</h5>

                                            <p class="mb-0">Anda juga bisa menjadi relawan, kordinator maupun saksi di
                                                TPS.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>
        </div>
</section>

<section class="timeline-section section-padding" id="section_3">
    <div class="section-overlay"
        style="background-image:linear-gradient(15deg, <?= $config->kodewarna ?> 0%, <?= $config->kodewarna ?> 100%)">
    </div>

    <div class="container">
        <div class="row justify-content-center">

            <div class="col-12 text-center">
                <h2 class="text-white mb-4">Daftar Mudah !</h1>
            </div>

            <div class="col-lg-10 col-12 mx-auto">
                <div class="timeline-container">
                    <ul class="vertical-scrollable-timeline" id="vertical-scrollable-timeline">
                        <div class="list-progress">
                            <div class="inner"></div>
                        </div>

                        <li>
                            <h4 class="text-white mb-3">Dapatkan Kartu</h4>

                            <p class="text-white">Anda bisa memperoleh Kartu {{$config->nama_kartu}} melalui caleg atau
                                Timses kami</p>

                            <div class="icon-holder">
                                <i class="bi-search"></i>
                            </div>
                        </li>

                        <li>
                            <h4 class="text-white mb-3">Daftarkan Kartu</h4>

                            <p class="text-white">Setelah berhasil memperoleh kartu daftarkan pada website ini. </p>

                            <div class="icon-holder">
                                <i class="bi-bookmark"></i>
                            </div>
                        </li>

                        <li>
                            <h4 class="text-white mb-3">Jalankan Usaha</h4>

                            <p class="text-white">Setelah proses pendaftaran selesai, Anda bisa langsung menjadi agen
                                PPOB, membuat website dan banyak lagi.</p>

                            <div class="icon-holder">
                                <i class="bi-book"></i>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>


        </div>
    </div>
</section>

@endsection