<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="og:title" content="Kartu {{ $config->nama_kartu }}">
    <meta property="og:description" content="Kartu Program {{ $config->nama_kartu }}">
    <meta property="og:image" content="{{ asset('assets/img/') . '/' . $config->favicon }}">
    <meta property="og:url" content="{{ url('/') }}">
    <meta property="og:type" content="website">


    <!-- Meta tag khusus untuk WhatsApp -->
    <meta property="al:android:url" content="{{ asset('assets/img/') . '/' . $config->favicon }}">
    <meta property="al:android:package" content="com.whatsapp">
    <meta property="al:android:app_name" content="WhatsApp">
    <!-- Meta tag khusus untuk Twitter -->

    <meta name="twitter:card" content="{{ asset('assets/img/') . '/' . $config->favicon }}">
    <meta name="twitter:title" content="Kartu {{ $config->nama_kartu }}">
    <meta name="twitter:description" content="Kartu Program {{ $config->nama_kartu }}">
    <meta name="twitter:image" content="{{ asset('assets/img/') . '/' . $config->favicon }}">


    <title>Kartu {{ $config->nama_kartu }}</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
    <link rel="icon" type="image/png" href="{{ asset('assets/img') }}/{{ $config->favicon }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        #card2 {
            display: none;
        }

        .custom-input-file::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-input-file {
            z-index: 10;
        }

        .custom-input-file::before {
            text-align: center;
            width: 100%;
            height: 100%;
            content: ' ';
            display: inline-block;
            background-color: #ffffff00;
            border: 3px solid #000000;
            border-radius: 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
        }

        .custom-input-file:hover::before {
            background-color: rgba(0, 0, 0, 0.025);
        }

        .custom-input-group {
            position: relative;
        }

        .custom-label {
            z-index: 0;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%)
        }
    </style>
</head>

<body style="background-image: {{ $config->color_gradient }}">
    <section>
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-8 col-xl-6 col-11">
                <div class="card rounded-3 border-0 shadow-lg mb-3 p-3 mt-4"
                    style="background-color: {{ $config->kodewarna }}; color: white; font-size: 16px">


                    <div class="row justify-content-center">
                        <div class="col-3 text-center">
                            <img class="mx-auto rounded-circle"
                                src="{{ asset('/') }}assets/img/{{ $config->logo }}" width="80px" height="80px"
                                alt="">
                        </div>
                        <div class="col-12 text-center mt-2 fw-bold fs-6 text-capitalize" style="line-height:1.1">

                            <h3>
                                {{ $config->sebutan }}
                            </h3>
                            {{ $config->dapil }}
                            <br>
                            {{ $config->daerah }}

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-lg-8 col-xl-6 col-11">
                <div class="card rounded-3 shadow-sm">

                    <div class="card-body p-4 p-md-5" id="targetElement">
                        <h3 class="mb-2 pb-2 pb-md-0 px-md-2 text-center">Registrasi Kartu {{ $config->nama_kartu }}
                        </h3>
                        <p>
                            Mohon isi data berikut dengan benar.
                        </p>
                        <hr>
                        <form class="px-md-2" action="{{ route('simpanPendaftaran') }}" id="regForm" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="form-outline mb-3">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <div class="form-outline mb-3">
                                @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-outline mb-3">
                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif
                            </div>

                            <div class="cards border-0" id="card1">
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="nama">Nama Lengkap</label>
                                    <input type="text" id="nama" name="name" class="form-control" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">Nomor Kartu
                                        {{ $config->nama_kartu }}</label>
                                    <input type="text" id="form2Example1q" name="kode_kartu" class="form-control" />
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">No. Whatsapp</label>
                                    <input type="number" id="form2Example1q" name="no_hp" class="form-control" />
                                </div>



                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form4Example1q">Email</label>
                                    <input type="email" id="form4Example1q" name="email" class="form-control" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form4Example1q">Password</label>
                                    <input type="password" id="form4Example1q" name="password"
                                        class="form-control" />
                                </div>


                                <div class="input-group mb-3">
                                    <input type="file" class="form-control" id="ktpfoto" name="ktp">
                                    <label class="input-group-text" for="ktpfoto">Foto KTP</label>
                                </div>


                                <div class="d-grid gap-2 mt-2 mb-2 mt-3">
                                    <button type="button" class="btn btn-dark btn-lg mb-1"
                                        onclick="showCard('card2')">Selanjutnya</button>


                                </div>
                            </div>



                            <div class="cards border-0" id="card2">


                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form5Example1q">Alamat Rumah</label>
                                    <textarea type="email" id="form5Example1q" name="alamat" class="form-control" cols="30" rows="3"></textarea>
                                </div>

                                <div class="form-outline mb-3">

                                    <select class="form-select" id="form6Example1q" name="provinsi"
                                        onchange="getKota(this)">
                                        <option value="" selected disabled>PROVINSI</option>
                                        @foreach ($provinsi as $prov)
                                            <option value="{{ $prov['id'] }}|{{ $prov['name'] }}">
                                                {{ $prov['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-outline mb-3">

                                    <select class="form-select" id="kabKota" name="kota"
                                        onchange="getKec(this)">

                                        <option value="" selected disabled>KOTA</option>

                                    </select>
                                </div>

                                <div class="form-outline mb-3">

                                    <select class="form-select" id="kabKec" name="kecamatan"
                                        onchange="getKel(this)">
                                        <option value="">KECAMATAN</option>
                                    </select>
                                </div>

                                <div class="form-outline mb-3">

                                    <select class="form-select" id="kabKel" name="kelurahan">
                                        <option value="">KELURAHAN</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-outline mb-3">

                                            <input type="text" name="rt" class="form-control"
                                                placeholder="RT">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-outline mb-3">

                                            <input type="text" name="rw" class="form-control"
                                                placeholder="RW">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-outline mb-3">

                                            <input type="text" name="tps" class="form-control"
                                                placeholder="No TPS">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <a href="https://cekdptonline.kpu.go.id/"
                                            class="btn "style="color:white;background-color:{{ $config->kodewarna }}">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            <span class="fw-5 fw-bold">
                                                Cek No. TPS
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-12">
                                        <div class="container text-white p-3 rounded-2 shadow-sm"
                                            style="background-color: {{ $config->kodewarna }};">
                                            <b>Apakah anda bersedia menjadi Relawan, Koordinator, atau Saksi untuk
                                                {{ $config->sebutan }} di pemilu 2024
                                                :</b>
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio"
                                                            name="bersedia" id="bersedia" value="bersedia" />
                                                        <label class="form-check-label"
                                                            for="bersedia">Bersedia</label>
                                                    </div>

                                                    <div class="container" id="pilihan_koordinator"
                                                        style="display:none">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="koordinator" id="relawan" value="Relawan" />
                                                            <label class="form-check-label"
                                                                for="relawan">Relawan</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="koordinator" id="koordinator"
                                                                value="Koordinator" />
                                                            <label class="form-check-label"
                                                                for="koordinator">Koordinator</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="koordinator" id="Saksi" value="Saksi" />
                                                            <label class="form-check-label"
                                                                for="Saksi">Saksi</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="koordinator" id="Lainnya" value="Lainnya" />
                                                            <label class="form-check-label"
                                                                for="Lainnya">Lainnya</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio"
                                                            name="bersedia" value="" id="tidakbersedia" />
                                                        <label class="form-check-label" for="tidakbersedia">Tidak,
                                                            Hanya
                                                            Simpatisan</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">


                                        <div class="d-grid gap-2 mt-2 mb-2 mt-3">
                                            <button type="button" class="btn btn-secondary btn-lg mb-1"
                                                onclick="showCard('card1')">Sebelumnya</button>
                                            <button type="submit"
                                                class="btn btn-dark btn-lg mb-1">Registrasi</button>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center mt-3">
            <div class="col-lg-8 col-xl-6 col-11 mb-3">
                <div class="card card-body rounded-3 shadow-sm text-center">
                    <center>
                        <img src="{{ asset('assets/img/Logo_Kartu Caleg.png') }}" style="height:auto;width:200px;"
                            class="mb-2">
                    </center>

                    <p class="text-dark fw-bold" style="font-size:14px">
                        IS Plaza Ballrom Lt.5
                        Jl. Pramuka No.150, RT.9/RW.5, Utan Kayu Utara, Kec. Matraman,<br>
                        Jakarta, Daerah Khusus Ibukota
                        Jakarta 13120
                        <br>
                        +62 812 3575 7667
                        <br>
                        swarasemesta.id@gmail.com
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"
        integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script>
    <script>
        const form = document.getElementById('multiStepForm');

        function showCard(cardId) {
            const cards = document.querySelectorAll('.cards');
            cards.forEach(card => {
                card.style.display = 'none';
            });
            const selectedCard = document.getElementById(cardId);
            selectedCard.style.display = 'block';
        }

        // document.getElementById("scrollButton").addEventListener("click", function () {
        //     // Menggulir ke elemen target
        //     document.getElementById("targetElement").scrollIntoView({
        //         behavior: "smooth"
        //     });
        // });



        $('.collapse-custom').on('click', function() {
            let id = $(this).data('id');
            $($('#collapseVisiMisi').find(`.collapse.show`).not(`#${id}`)).removeClass('show')
        });


        $(document).on('click', 'input[name=bersedia]', function() {
            if ($(`input#bersedia`).is(':checked')) {
                $('div#pilihan_koordinator').show(10)
            } else {
                $('div#pilihan_koordinator').hide(10)
            }
        });



        function getKota(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{ url('') }}/get-api/getKota/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKota');
                    $select.html("");
                    $.each(response, function(index, item) {


                        var $option = $('<option>').val(`${item.id}|${item.name}`).text(
                            item.name);
                        $select.append($option);


                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }

        function getKec(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{ url('') }}/get-api/getKec/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKec');
                    $select.html("");
                    $.each(response, function(index, item) {


                        var $option = $('<option>').val(`${item.id}|${item.name}`).text(
                            item.name);
                        $select.append($option);

                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }

        function getKel(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{ url('') }}/get-api/getKel/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKel');
                    $select.html("");
                    $.each(response, function(index, item) {

                        var $option = $('<option>').val(`${item.id}|${item.name}`).text(
                            item.name);
                        $select.append($option);

                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }
    </script>

</body>

</html>
