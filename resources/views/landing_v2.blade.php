<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">

    <style>
        @media (min-width: 1025px) {
            .h-custom {
                height: ss 100vh !important;
            }
        }

        .custom-input-file::-webkit-file-upload-button {
            visibility: hidden;
        }

        .custom-input-file {
            z-index: 10;
        }

        .custom-input-file::before {
            text-align: center;
            width: 100%;
            height: 100%;
            content: ' ';
            display: inline-block;
            background-color: #ffffff00;
            border: 3px solid #000000;
            border-radius: 8px;
            outline: none;
            white-space: nowrap;
            -webkit-user-select: none;
            cursor: pointer;
            text-shadow: 1px 1px #fff;
        }

        .custom-input-file:hover::before {
            background-color: rgba(0, 0, 0, 0.025);
        }

        .custom-input-group {
            position: relative;
        }

        .custom-label {
            z-index: 0;
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%)
        }
    </style>
</head>

<body>
    <section class="h-100 h-custom" style="background-color: #8fc4b7;">
        <div class="container py-5 h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-lg-8 col-xl-6">
                    <div class="card rounded-3">
                        <img src="{{asset('/')}}assets/img/kartu-1.jpg" class="w-100" style="border-top-left-radius: .3rem; border-top-right-radius: .3rem;" alt="Sample photo">
                        <div class="card-body p-4 p-md-5">
                            <h3 class="mb-3 pb-2 pb-md-0 px-md-2 text-center">Pendaftar KTP {{$config->nama_pasangan}}</h3>

                            <h6 class="mb-4 px-md-2 text-center">
                                <a href="#" data-bs-toggle="modal" data-bs-target="#exampleModal">Sudah Daftar? Klik
                                    untuk login!</a>
                            </h6>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Login</h1>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <form action="">

                                            <div class="modal-body">
                                                <div class="form-group mb-3">
                                                    <label for="id-ktp">ID KTP SANSON</label>
                                                    <input class="form-control" type="text" name="" id="id-ktp" placeholder="Ketik Disini...">
                                                </div>

                                                <div class="form-group mb-3">
                                                    <label for="password">PASSWORD</label>
                                                    <input class="form-control" type="password" name="" id="password" placeholder="Ketik Disini...">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Login</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>

                            <form class="px-md-2" action="{{route('simpanPendaftaran')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-outline mb-3">
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>

                                <div class="form-outline mb-3">
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('error') }}
                                    </div>
                                    @endif
                                </div>
                                <div class="form-outline mb-3">
                                    @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form1Example1q">Nama Lengkap</label>
                                    <input type="text" id="form1Example1q" name="name" class="form-control" />
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">No. HP</label>
                                    <input type="number" id="form2Example1q" name="no_hp" class="form-control" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">No. KTP</label>
                                    <input type="number" id="form2Example1q" name="no_ktp" class="form-control" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">No. Kartu Keluarga</label>
                                    <input type="number" id="form2Example1q" name="no_kk" class="form-control" />
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form2Example1q">Nomor Seri Kartu {{$config->nama_kartu}}</label>
                                    <input type="text" id="form2Example1q" name="kode_kartu" class="form-control" />
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form4Example1q">Email</label>
                                    <input type="email" id="form4Example1q" name="email" class="form-control" />
                                </div>


                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form5Example1q">Alamat Rumah</label>
                                    <textarea type="email" id="form5Example1q" name="alamat" class="form-control" cols="30" rows="3"></textarea>
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form6Example1q">Provinsi</label>
                                    <select class="form-select" id="form6Example1q" name="provinsi" onchange="getKota(this)">
                                        <option value="" selected disabled>PROVINSI</option>
                                        @foreach ($provinsi as $prov )
                                        @foreach ($prov as $pv)
                                        <option value="{{$pv['id']}}|{{$pv['nama']}}">{{$pv['nama']}}</option>
                                        @endforeach
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form7Example1q">Kab/Kota</label>
                                    <select class="form-select" id="kabKota" name="kota" onchange="getKec(this)">

                                        <option value="" selected disabled>KOTA</option>

                                    </select>
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form8Example1q">Kecamatan</label>
                                    <select class="form-select" id="kabKec" name="kecamatan" onchange="getKel(this)">
                                        <option value="">KECAMATAN</option>
                                    </select>
                                </div>

                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form9Example1q">Kelurahan</label>
                                    <select class="form-select" id="kabKel" name="kelurahan">
                                        <option value="">KELURAHAN</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label" for="form10Example1q">RT</label>
                                            <input type="text" name="rt" class="form-control" placeholder="RT">
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-outline mb-3">
                                            <label class="form-label" for="form11Example1q">RW</label>
                                            <input type="text" name="rw" class="form-control" placeholder="RW">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-outline mb-3">
                                    <label class="form-label" for="form4Example1q">Password</label>
                                    <input type="password" id="form4Example1q" name="password" class="form-control" />
                                </div>


                                <div class="row mb-3">
                                    <div class="col-12">
                                        <div class="container text-white p-3 rounded-2 shadow-sm"style="background-color: {{$config->kodewarna}};">
                                            <b>Apakah Anda bersedia menjadi pengawas kecurangan Pemilu 2023 bagi {{$config->nama_pasangan}}
                                                :</b>
                                            <div class="row">
                                                <div class="col-12 mb-2">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="bersedia" id="bersedia" value="bersedia" />
                                                        <label class="form-check-label" for="bersedia">Bersedia</label>
                                                    </div>

                                                    <div class="container" id="pilihan_koordinator"style="display:none">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="koordinator"id="relawan"  value="Relawan"  />
                                                            <label class="form-check-label" for="relawan">Relawan</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="koordinator"id="koordinator"  value="Koordinator" />
                                                            <label class="form-check-label" for="koordinator">Koordinator</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"  name="koordinator"id="Saksi"  value="Saksi" />
                                                            <label class="form-check-label" for="Saksi">Saksi</label>
                                                        </div>
                                                    </div>

                                                    
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="bersedia" value=""id="tidakbersedia" />
                                                        <label class="form-check-label" for="tidakbersedia">Tidak, Hanya Simpatisan</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">

                                        <div class="row mb-3 mt-3">
                                            <div class="col-6">
                                                <div class="input-group custom-input-group"style="height:70px">
                                                    <input class="custom-input-file" type="file" id="inputGroupFile02" name="ktp" style="color: transparent" />
                                                    <b class="custom-label" for="inputGroupFile02">Upload KTP</b>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="input-group custom-input-group"style="height:70px">
                                                    <input class="custom-input-file" type="file" id="kk" name="kk" style="color: transparent" />
                                                    <b class="custom-label" for="kk">Upload KK</b>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-success btn-lg mb-1">Registrasi</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js" integrity="sha384-fbbOQedDUMZZ5KreZpsbe1LCZPVmfTnH7ois6mU1QK+m14rQ1l2bGBq41eYeM/fS" crossorigin="anonymous">
    </script>

    <script>

        $(document).on('click','input[name=bersedia]',function(){
            if ($(`input#bersedia`).is(':checked')) {
                $('div#pilihan_koordinator').show(10)
            }else{
                $('div#pilihan_koordinator').hide(10)
            }
        });


        function getKota(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{url('')}}/get-api/getKota/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKota');
                    $select.html("");
                    $.each(response, function(index, items) {
                        $.each(items, function(index, item) {

                            var $option = $('<option>').val(`${item.id}|${item.nama}`).text(item.nama);
                            $select.append($option);
                        })

                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }

        function getKec(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{url('')}}/get-api/getKec/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKec');
                    $select.html("");
                    $.each(response, function(index, items) {
                        $.each(items, function(index, item) {
                            console.log(item)
                            var $option = $('<option>').val(`${item.id}|${item.nama}`).text(item.nama);
                            $select.append($option);
                        });
                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }

        function getKel(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{url('')}}/get-api/getKel/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKel');
                    $select.html("");
                    $.each(response, function(index, items) {
                        $.each(items, function(index, item) {
                            var $option = $('<option>').val(`${item.id}|${item.nama}`).text(item.nama);
                            $select.append($option);
                        });
                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }
    </script>

</body>

</html>