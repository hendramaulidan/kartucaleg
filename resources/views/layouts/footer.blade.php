<footer class="c-footer">
    <div><a href="https://coreui.io">CoreUI</a> © Swasem.</div>
    <div class="ml-auto">Powered by&nbsp;<a href="https://coreui.io/">CoreUI</a></div>
  </footer>
</div>
</div>
<!-- CoreUI and necessary plugins-->

<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM="
        crossorigin="anonymous"></script>

<script src="{{asset('/')}}vendors/@coreui/coreui/js/coreui.bundle.min.js"></script>
<!--[if IE]><!-->
<script src="{{asset('/')}}vendors/@coreui/icons/js/svgxuse.min.js"></script>
<!--<![endif]-->
<!-- Plugins and scripts required by this view-->
<script src="{{asset('/')}}vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js"></script>
<script src="{{asset('/')}}vendors/@coreui/utils/js/coreui-utils.js"></script>
<script src="{{asset('/')}}js/main.js"></script>
@livewireScripts

<script>
    function getKota(prov) {
            let idProv = $(prov).val()
            // console.log(idProv)
            idProv = idProv.split("|")[0];
            $.ajax({
                url: `{{ url('') }}/get-api/getKota/${idProv}`,
                method: 'GET',
                dataType: 'json',
                success: function(response) {
                    var $select = $('#kabKota');
                    $select.html("");
                    $.each(response, function(index, item) {


                        var $option = $('<option>').val(`${item.name}`).text(item.name);
                        $select.append($option);


                    });
                },
                error: function(xhr, status, error) {
                    // Tangani kesalahan
                    console.log('Error:', error);
                }
            });
        }
</script>
</body>
</html>