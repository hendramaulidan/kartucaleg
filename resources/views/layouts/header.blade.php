<div class="c-wrapper c-fixed-components">
  <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
      <svg class="c-icon c-icon-lg">
        <use xlink:href="{{asset('/')}}vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
      </svg>
    </button><a class="c-header-brand d-lg-none" href="#">
      <svg width="118" height="46" alt="CoreUI Logo">
        <use xlink:href="{{asset('/')}}assets/brand/coreui.svg#full"></use>
      </svg></a>
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
      <svg class="c-icon c-icon-lg">
        <use xlink:href="{{asset('/')}}vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
      </svg>
    </button>
    <ul class="c-header-nav d-md-down-none">
      <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">Dashboard</a></li>
    </ul>
    <ul class="c-header-nav ml-auto mr-4">

      <li class="c-header-nav-item dropdown"><a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="c-avatar"><img class="c-avatar-img" src="https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png"></div>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">

          <div class="dropdown-divider"></div>
          <form action="{{ route('logout') }}" method="POST">
            <button class="dropdown-item"type="submit">
              @csrf
              <svg class="c-icon mr-2" >
                <use xlink:href="{{asset('/')}}vendors/@coreui/icons/svg/free.svg#cil-account-logout"></use>
              </svg> Logout
            </button>
          </form>
         
        </div>
      </li>
    </ul>

  </header>
  <div class="c-body">