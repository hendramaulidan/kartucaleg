<body class="c-app">
    <div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
        <div class="c-sidebar-brand d-lg-down-none p-2 ">
            <img src="{{ asset('assets/img/logo_nav.png') }}" class="img-fluid mb-3"style="width:150px;height:auto">
        </div>
        <ul class="c-sidebar-nav">


            <li class="c-sidebar-nav-title">Admin</li>



            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.home') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-speedometer"></use>
                    </svg> Dashboard</a>
            </li>


            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.ppob') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-home"></use>
                    </svg>PPOB</a>
            </li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.web-request') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-globe-alt"></use>
                    </svg> Website Reguler</a>
            </li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.webRequestPremium') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-globe-alt"></use>
                    </svg> Website Premium</a>
            </li>

            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.brosur-digital') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-notes"></use>
                    </svg>Brosur Digital</a>
            </li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.merchan') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-3d"></use>
                    </svg>Merchant</a>
            </li>

            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.webinar') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-globe-alt"></use>
                    </svg>Webinar</a>
            </li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.program-menarik') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="{{ asset('/') }}vendors/@coreui/icons/svg/free.svg#cil-list-rich"></use>
                    </svg>Bantuan Caleg</a>
            </li>

            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a
                    class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-puzzle"></use>
                    </svg>Wa Broadcast</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.unduhData') }}"><span class="c-sidebar-nav-icon"></span> Unduh
                            Nomor</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('landing.broadcast') }}"target="_blank"><span
                                class="c-sidebar-nav-icon"></span> Broadcast</a></li>


                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a
                    class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-puzzle"></use>
                    </svg>Edit Fitur</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.dapilKota') }}"><span class="c-sidebar-nav-icon"></span> Kota</a>
                    </li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.jenisToko') }}"><span class="c-sidebar-nav-icon"></span> Jenis
                            Toko</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{ route('admin.toko') }}"><span
                                class="c-sidebar-nav-icon"></span> Toko</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.jenisUsaha') }}"><span class="c-sidebar-nav-icon"></span> Jenis
                            Usaha</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.jenisBantuan') }}"><span class="c-sidebar-nav-icon"></span> Bantuan
                            Caleg</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.temaWork') }}"><span class="c-sidebar-nav-icon"></span> Tema
                            Workshop</a></li>

                </ul>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown"><a
                    class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle">
                    <svg class="c-sidebar-nav-icon">
                        <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-puzzle"></use>
                    </svg>Dokumentasi</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('dok.marketing') }}"><span class="c-sidebar-nav-icon"></span>Publikasi</a>
                    </li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('dok.kartuCaleg') }}"><span class="c-sidebar-nav-icon"></span>Distribusi Kartu</a></li>


                </ul>
            </li>

        </ul>
        <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
            data-class="c-sidebar-minimized"></button>
    </div>
