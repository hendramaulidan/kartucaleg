<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>

                <th>No</th>
                <th>Jenis</th>
                <th>Action</th>


            </tr>
        </thead>
        <tbody>
            <?php $i = 1; ?>
            @foreach ($users as $user)
                <tr>

                    <th>
                        {{ $i++ }}
                    </th>
                    <td>
                        {{ $user->kota }}

                    </td>
                    <td>
                        <a href="#"data-toggle="modal" data-target="#modalDelete{{$user->id}}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>


                <!-- Modal -->
                <div class="modal fade" id="modalDelete{{$user->id}}" tabindex="-1" aria-labelledby="modalDelete{{$user->id}}Label"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalDelete{{$user->id}}Label">Delete Kota {{ $user->kota }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{ route('admin.hapusDapilKota') }}" method="post">
                                    @csrf
                                <div class="modal-body">

                                    <div class="form-outline mb-3">
                                        <h5> Password <br></h5>
                                        <input type="hidden" name="id"value="{{$user->id}}">
                                        <input type="password" name="password"
                                            class="form-control"placeholder="password">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>
