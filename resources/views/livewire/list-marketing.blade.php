<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>

                <th>No</th>

                <th>Deskripsi</th>
                <th>Alamat</th>
                <th>Action</th>


            </tr>
        </thead>
        <tbody>
            <?php
            
            $i = 1; ?>
            @foreach ($users as $user)
                <tr>

                    <th>
                        {{ $i++ }}
                    </th>
                    <td> 
                        {{$user->deskripsi}}
                    </td>
                    <td> 
                        {{$user->alamat}}
                    </td>
                    <td>
                        <a href="{{ route('admin.detailMarketing', $user->id) }}" class="btn btn-info">Detail</a>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>
