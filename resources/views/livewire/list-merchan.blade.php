<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>
                <th>Nama</th>
                <th>Dapil</th>
                <th>Jenis Toko</th>
                <th>Toko</th>
                <th>Status</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user )
            <tr>
                
                <td>
                    {{$user->nama}}
                </td>
                <td>
                    {{$user->dapil}}

                </td>
             
                <td>
                    {{$user->jenis_toko}}
                </td>
                <td>
                    {{$user->toko}}
                </td>
                <td>
                  @if ($user->status == 0)
                            <div class="badge badge-secondary">
                                &nbsp;&nbsp;Menunggu&nbsp;&nbsp;
                            </div>
                        @elseif ($user->status == 1)
                            <div class="badge badge-primary">
                                &nbsp;&nbsp;Sedang Di Proses&nbsp;&nbsp;
                            </div>
                        @else
                            <div class="badge badge-success">
                                &nbsp;&nbsp;Selesai&nbsp;&nbsp;
                            </div>
                        @endif
                </td>
                <td>
                    <div class="form-inline">
                        <a href="{{route('admin.detailMerchan',$user->id)}}" class="btn btn-info">Detail</a>
    
                        
                        @if ($user->status == 0)
                                @elseif ($user->status == 1)
                                    <form action="{{ route('admin.updateMerchan', $user->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="status"value="2">
                                        <button type="submit"class="btn btn-dark">Selesaikan Proses</button>
                                    </form>
                                @else
                                @endif
                    </div>

                </td>
            </tr>

            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>