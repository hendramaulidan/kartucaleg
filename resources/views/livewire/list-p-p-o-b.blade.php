<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>

                <th>Nama</th>
                <th>Email</th>
                <th>No. Hp</th>
                <th>Hubungi</th>
                <th>Status</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>

                    <td>
                        {{ $user->nama }}

                    </td>
                    <td>
                        {{ $user->email }}

                    </td>

                    <td>
                        {{ $user->no_hp }}
                    </td>
                    <td>
                        <a href="https://wa.me/{{ $user->no_hp }}" target="_blank"
                            rel="noopener noreferrer"class="btn btn-success">Whatsapp</a>
                    </td>

                    <td>
                        @if ($user->status == 0)
                            <div class="badge badge-secondary">
                                &nbsp;&nbsp;Menunggu&nbsp;&nbsp;
                            </div>
                        @elseif ($user->status == 1)
                            <div class="badge badge-primary">
                                &nbsp;&nbsp;Sedang Di Proses&nbsp;&nbsp;
                            </div>
                        @else
                            <div class="badge badge-success">
                                &nbsp;&nbsp;Selesai&nbsp;&nbsp;
                            </div>
                        @endif
                    </td>


                    <td>
                        <div class="form-inline">
                            <a href="{{ route('admin.detailPpob', $user->id) }}" class="btn btn-info mr-4">Detail</a>
                            @if ($user->status == 0)
                          
                            @elseif ($user->status == 1)
                                <form action="{{ route('admin.updatePpob', $user->id) }}" method="post">
                                    @csrf
                                    <input type="hidden" name="status"value="2">
                                    <button type="submit"class="btn btn-dark">Selesaikan Proses</button>
                                </form>
                            @else
                            @endif
                        </div>

                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>
