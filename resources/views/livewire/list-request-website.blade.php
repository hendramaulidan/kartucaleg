<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>

                <th>Nama</th>

                <th>Model Usaha</th>
                <th>SubDomain / Domain</th>
                <th>Jenis Usaha</th>
          
                <th>Nomor Seri Kartu {{ $config->nama_kartu }}</th>
                <th>Hubungi</th>
                <th>status</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>

                    <td>
                        {{ $user->user->name }}

                    </td>

                    <td>
                        {{ $user->model_usaha }}
                    </td>
                    <td>
                        {{ $user->subdomain }}
                    </td>

                    <td>
                        {{ $user->jenis_usaha }}
                    </td>



                    <!-- <td>
                    {{ $user->alamat_usaha }}
                </td>
                
                <td>
                    {{ $user->deskripsi }}
                </td>

                <td>
                    {{ $user->jenis_domain }}
                </td> -->

                    <td>
                        {{ $user->user->kode_kartu }}
                    </td>

                    <td>
                        <a href="https://wa.me/{{ $user->no_hp }}" target="_blank"
                            rel="noopener noreferrer"class="btn btn-success">Whatsapp</a>
                    </td>

                    <td>

                        @if ($user->status == 0)
                            <div class="badge badge-secondary">
                                &nbsp;&nbsp;Menunggu&nbsp;&nbsp;
                            </div>
                        @elseif ($user->status == 1)
                            <div class="badge badge-primary">
                                &nbsp;&nbsp;Sedang Di Proses&nbsp;&nbsp;
                            </div>
                        @else
                            <div class="badge badge-success">
                                &nbsp;&nbsp;Selesai&nbsp;&nbsp;
                            </div>
                        @endif


                    </td>
                    <td>
                        <div class="form-inline">

                            <a href="{{ route('admin.detailWebRequest', $user->id) }}" class="btn btn-info">Detail</a>
                            @if ($user->status == 0)
                              
                              @elseif ($user->status == 1)
                                  <form action="{{ route('admin.updateReqWeb', $user->id) }}" method="post">
                                      @csrf
                                      <input type="hidden" name="status"value="2">
                                      <button type="submit"class="btn btn-dark">Selesaikan Proses</button>
                                  </form>
                              @else
                              @endif
                        </div>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>
