<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>

                <th>No</th>

                <th>Jenis Workshop</th>
                <th>Tema Workshop</th>
                <th>Action</th>


            </tr>
        </thead>
        <tbody>
            <?php
            
            $i = 1; ?>
            @foreach ($users as $user)
                <tr>

                    <th>
                        {{ $i++ }}
                    </th>
                    <td>
                        

                            {!! $user->tipe == 1
                                ? '<span class="badge badge-success">Online</span>'
                                : '<span class="badge badge-danger">Offline</span>' !!}
                        
                    </td>



                    <td>
                    {{$user->tema }}
                    </td>
                  
                    <td>
                        <a href="{{ route('admin.hapusTemaWork', $user->id) }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>
