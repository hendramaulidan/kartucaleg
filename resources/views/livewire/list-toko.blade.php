<div>
    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>
               
                <th>No</th>
                <th>NamaToko</th>
                <th>Jenis Toko</th>
                <th>Nama Toko</th>
                <th>Action</th>
              

            </tr>
        </thead>
        <tbody>
                <?php



 $i =1; ?>
            @foreach ($users as $user )
            <tr>
                
                <th>
                    {{$i++}}
                </th>
                <td>
                    {{$user->toko}}
                </td>
                <?php
                 $jenis =  App\Models\JenisToko::where('id',$user->id_jenis)->first();
                 
                 ?>
                <td>
                    {{$jenis->jenis}}
                </td>
                <?php
                 $kota =  App\Models\DapilKota::where('id',$user->id_kota)->first();
                 
                 ?>
                <td>
                    {{$kota->kota}}
                </td>
              
                
                <td>
                    <a href="{{route('admin.hapusToko',$user->id)}}" class="btn btn-danger">Hapus</a>
                </td>
            </tr>

            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>