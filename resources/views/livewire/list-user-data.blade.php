<div>

    <table class="table table-responsive-sm table-hover table-outline mb-0">
        <thead class="thead-light">
            <tr>
               
                <th>Nama</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>Nomor Seri Kartu {{$config->nama_kartu}}</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user )
            <tr>
                <td>
                    {{$user->name}}

                </td>
                <td>
                    {{$user->email}}

                </td>
                @if ($user->userPublic)
                <td>
                    {{$user->userPublic->alamat}}

                </td>
                @else
                <td>


                </td>
               
                @endif
                <td>
                    {{$user->kode_kartu}}
                </td>
                <td>
                    <a href="{{route('admin.detail-user',$user->id)}}" class="btn btn-success">
                        Detail
                    </a>
                </td>
            </tr>

            @endforeach

        </tbody>
    </table>

    <div class="container mt-2">
        <div class="row">
            <div class="col-12">
                {{ $users->links('vendor.livewire.bootstrap') }}
            </div>
        </div>
    </div>
</div>