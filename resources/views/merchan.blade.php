@extends('layouts.main')
@section('content')

<main class="c-main">
    <div class="container-fluid">
        <div class="fade-in">

            <h1 class="mb-3">List Merchant</h1>

            <!-- /.row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        
                        <div class="card-body">
                          <livewire:list-merchan/>
                        </div>
                     
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- /.row-->
        </div>
    </div>
</main>
@endsection
