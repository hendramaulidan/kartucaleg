</main>
<footer class="site-footer p-4" style="border-bottom:10px solid <?= $config->kodewarna ?>;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-4 col-12 text-center ">
          
            <img src="{{asset('assets/img/Logo_Kartu Caleg.png')}}"
                    style="height:60px;width:auto;"class="mb-2">
                <p class="text-dark fw-bold" style="font-size:14px">
                    IS Plaza Ballrom Lt.5
                    Jl. Pramuka No.150, RT.9/RW.5, Utan Kayu Utara, Kec. Matraman,<br> 
                    Jakarta, Daerah Khusus Ibukota
                    Jakarta 13120
                    <br>
                    +62 812 3575 7667
                    <br>
                    swarasemesta.id@gmail.com
                </p>
            </div>
        </div>
    </div>
</footer>


<!-- JAVASCRIPT FILES -->
<script src="{{asset('')}}landing/js/jquery.min.js"></script>
<script src="{{asset('')}}landing/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('')}}landing/js/jquery.sticky.js"></script>
<script src="{{asset('')}}landing/js/click-scroll.js"></script>
<script src="{{asset('')}}landing/js/custom.js"></script>

</body>

</html>