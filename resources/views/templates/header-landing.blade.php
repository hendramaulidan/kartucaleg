<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Kartu {{$config->nama_kartu}}</title>


    <meta property="og:title" content="Kartu {{$config->nama_kartu}}">
    <meta property="og:description" content="Kartu Program {{$config->nama_kartu}}">
    <meta property="og:image" content="{{asset('assets/img/').'/'.$config->favicon}}">
    <meta property="og:url" content="{{url('/')}}">
    <meta property="og:type" content="website">

      
    <!-- Meta tag khusus untuk WhatsApp -->
    <meta property="al:android:url" content="{{asset('assets/img/').'/'.$config->favicon}}">
    <meta property="al:android:package" content="com.whatsapp">
    <meta property="al:android:app_name" content="WhatsApp">
    <!-- Meta tag khusus untuk Twitter -->

    <meta name="twitter:card" content="{{asset('assets/img/').'/'.$config->favicon}}">
    <meta name="twitter:title" content="Kartu {{$config->nama_kartu}}">
    <meta name="twitter:description" content="Kartu Program {{$config->nama_kartu}}">
    <meta name="twitter:image" content="{{asset('assets/img/').'/'.$config->favicon}}">

    <!-- CSS FILES -->
    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    
    <link rel="icon" type="image/x-icon" href="{{asset('assets/img')}}/{{$config->favicon}}">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700&family=Open+Sans&display=swap" rel="stylesheet">

    <link href="{{asset('')}}landing/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{asset('')}}landing/css/bootstrap-icons.css" rel="stylesheet">

    <link href="{{asset('')}}landing/css/templatemo-topic-listing.css" rel="stylesheet">
    <!--

TemplateMo 590 topic listing

https://templatemo.com/tm-590-topic-listing

-->

    <style>
        
        .site-footer::after {
            content: "";
            position: absolute;
            bottom: 0;
            right: 0;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0 0 100px 100px;
            border-color: transparent transparent <?= $config->kodewarna ?> transparent !important;
            pointer-events: none;
        }
        @media (min-width: 768px) {
        /* Gaya khusus untuk tampilan desktop */
        /* Misalnya, lebar layar minimal 768px */
        /* ... */
            .mobile{
                display:none;
            }
            .desktop{
                display: block;
            }
        }

        /* Gaya untuk perangkat Android */
        @media (max-width: 767px) {
        /* Gaya khusus untuk perangkat dengan lebar layar kurang dari 768px (perangkat Android) */
        /* ... */
            .mobile{
                display:block;
            }
            .desktop{
                display: none;
            }
        }
        .hover-card {
      transition: transform 0.3s, box-shadow 0.3s; /* Efek transisi pada hover */
    }

    .hover-card:hover {
      transform: translateY(-3px); /* Mendorong kartu ke atas saat dihover */
      box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3); /* Efek bayangan saat dihover */
    }
    </style>
</head>

<body id="top">

    <main>

        <nav class="navbar navbar-expand-lg" style="background-color:{{$config->kodewarna}};margin-bottom:-40px">
            <div class="container">
                <a class="navbar-brand text-white" href="{{url('')}}">
                {{$config->sebutan}}
                 
                </a>

                <div class="d-lg-none ms-auto me-4">
                    <a href="{{url('')}}/daftar" class="navbar-icon bi-person smoothscroll"></a>
                </div>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ms-lg-5 me-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_1">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_2">Manfaat Kartu</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_3">Langkah Daftar</a>
                        </li>

                    </ul>

                    <div class="d-none d-lg-block">
                        <a href="{{url('')}}/daftar" class="navbar-icon bi-person"></a>

                    </div>
                </div>
            </div>
        </nav>