<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\LandingPages;
use App\Models\UserWebsite;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExportAll;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware(['auth','user-access:admin'])->group(function ()
{
    Route::get('/admin/home', [AdminController::class,"dashboard"])->name('admin.home');
    Route::get('/admin/detail-user/{id}', [AdminController::class,"detailUser"])->name('admin.detail-user');
    Route::get('/admin/list-user', [AdminController::class,"listUser"])->name('admin.list-user');
    Route::get('/admin/web-request', [AdminController::class,"webRequest"])->name('admin.web-request');
    Route::get('/admin/web-request-premium', [AdminController::class,"webRequestPrem"])->name('admin.webRequestPremium');
    Route::get('/admin/detail-web-request/{id}', function ($id){
        $data['user'] = UserWebsite::where('id',$id)->first();
        return view('detail-web',$data);
    })->name('admin.detailWebRequest');
    Route::get('/admin/detail-web-request-premium/{id}', function ($id){
        $data['user'] = App\Models\UserWebsitePrem::where('id',$id)->first();

        return view('detail-web-premium',$data);
    })->name('admin.detailWebRequestPremium');
    Route::get('/admin/detail-brosur/{id}', function ($id){
        $data['user'] = App\Models\BrosurDigital::where('id',$id)->first();
        return view('detail-brosur',$data);
    })->name('admin.detailBrosur');

    Route::get('/admin/detail-merchan/{id}', function ($id){
        $data['user'] = App\Models\Merchan::where('id',$id)->first();
        return view('detail-merchan',$data);
    })->name('admin.detailMerchan');
    Route::get('/admin/detail-ppob/{id}', function ($id){
        $data['user'] = App\Models\PPOB::where('id',$id)->first();
        return view('detail-ppob',$data);
    })->name('admin.detailPpob');
    Route::get('/admin/detail-webinar/{id}', function ($id){
        $data['user'] = App\Models\Webinar::where('id',$id)->first();
        return view('detail-webinar',$data);
    })->name('admin.detailWebinar');

    Route::get('/admin/detail-bantuan/{id}', function ($id){
        $data['user'] = App\Models\ProgramMenarik::where('id',$id)->first();
        return view('detail-bantuan',$data);
    })->name('admin.detailBantuan');

    Route::get('/admin/detail-dokumentasi-marketing/{id}', function ($id){
        $data['user'] = App\Models\DokMarketing::where('id',$id)->first();
        return view('detail-marketing',$data);
    })->name('admin.detailMarketing');

    Route::get('/admin/detail-dokumentasi-kartu-caleg/{id}', function ($id){
        $data['user'] = App\Models\DokKartuCaleg::where('id',$id)->first();
        return view('detail-kartu-caleg',$data);
    })->name('admin.detailKartuCaleg');


    Route::get('/admin/unduh-database', function (){
        return Excel::download(new UsersExportAll, 'usersAll.xlsx');
    })->name('admin.unduhDatabase');


    Route::get('/admin/brosur-digital', [AdminController::class,"brosurDigital"])->name('admin.brosur-digital');
    Route::get('/admin/unduh-data', [AdminController::class,"unduhData"])->name('admin.unduhData');


    Route::get('/admin/merchan', [AdminController::class,"merchan"])->name('admin.merchan');
    Route::get('/admin/ppob', [AdminController::class,"PPOB"])->name('admin.ppob');
    
    Route::get('/admin/webinar', [AdminController::class,"Webinar"])->name('admin.webinar');
    Route::get('/admin/program-menarik', [AdminController::class,"programMenarik"])->name('admin.program-menarik');
    
    Route::get('/admin/dapil-kota', [AdminController::class,"dapilKota"])->name('admin.dapilKota');
    Route::get('/admin/jenis-toko', [AdminController::class,"jenisToko"])->name('admin.jenisToko');
    Route::get('/admin/toko', [AdminController::class,"toko"])->name('admin.toko');
    Route::get('/admin/jenis-usaha', [AdminController::class,"jenisUsaha"])->name('admin.jenisUsaha');
    Route::get('/admin/jenis-bantuan', [AdminController::class,"jenisBantuan"])->name('admin.jenisBantuan');
    Route::get('/admin/tema-workshop', [AdminController::class,"temaWork"])->name('admin.temaWork');
    //marketing
    Route::get('/dokumentasi/marketing', [AdminController::class,"marketing"])->name('dok.marketing');
    Route::get('/dokumentasi/kartu-caleg', [AdminController::class,"kartuCaleg"])->name('dok.kartuCaleg');

    Route::post('/admin/tambah-dapil-kota', [AdminController::class,"tambahDapilKota"])->name('admin.tambahDapilKota');
    Route::post('/admin/tambah-jenis-toko', [AdminController::class,"tambahJenisToko"])->name('admin.tambahJenisToko');
    Route::post('/admin/tambah-toko', [AdminController::class,"tambahToko"])->name('admin.tambahToko');
    Route::post('/admin/tambah-jenis-usaha', [AdminController::class,"tambahJenisUsaha"])->name('admin.tambahJenisUsaha');
    Route::post('/admin/tambah-jenis-Bantuan', [AdminController::class,"tambahJenisBantuan"])->name('admin.tambahJenisBantuan');
    Route::post('/admin/tambah-tema-workshop', [AdminController::class,"tambahTemaWork"])->name('admin.tambahTemaWork');
    Route::post('/admin/tambah-dokumentasi-kartu-caleg', [AdminController::class,"tambahDokumentasiKartuCaleg"])->name('admin.tambahDokumentasiKartuCaleg');
    Route::post('/admin/tambah-dokumentasi-marketing', [AdminController::class,"tambahDokumentasiMarketing"])->name('admin.tambahDokumentasiMarketing');





    Route::post('/admin/edit-status-req-website/{id}', [AdminController::class,"updateReqWeb"])->name('admin.updateReqWeb');
    Route::post('/admin/edit-status-req-website-premium/{id}', [AdminController::class,"updateReqWebPremium"])->name('admin.updateReqWebPremium');
    Route::post('/admin/edit-status-brosur/{id}', [AdminController::class,"updateBrosur"])->name('admin.updateBrosur');
    Route::post('/admin/edit-status-Merchan/{id}', [AdminController::class,"updateMerchan"])->name('admin.updateMerchan');
    Route::post('/admin/edit-status-Ppob/{id}', [AdminController::class,"updatePpob"])->name('admin.updatePpob');
    Route::post('/admin/edit-status-Webinar/{id}', [AdminController::class,"updateWebinar"])->name('admin.updateWebinar');
    Route::post('/admin/edit-status-Bantuan/{id}', [AdminController::class,"updateBantuan"])->name('admin.updateBantuan');

    Route::post('/admin/hapus-dapil-kota/', [AdminController::class,"hapusDapilKota"])->name('admin.hapusDapilKota');
    Route::get('/admin/hapus-jenis-toko/{id}', [AdminController::class,"hapusJenisToko"])->name('admin.hapusJenisToko');
    Route::get('/admin/hapus-toko/{id}', [AdminController::class,"hapusToko"])->name('admin.hapusToko');
    Route::get('/admin/hapus-jenis-usaha/{id}', [AdminController::class,"hapusJenisUsaha"])->name('admin.hapusJenisUsaha');
    Route::get('/admin/hapus-jenis-bantuan/{id}', [AdminController::class,"hapusJenisBantuan"])->name('admin.hapusJenisBantuan');
    Route::get('/admin/hapus-tema-workshop/{id}', [AdminController::class,"hapusTemaWork"])->name('admin.hapusTemaWork');
    


  


    


//     Route::get('/admin/list-user', function () {
//         return view('list-user');
//     })->name('admin.list-user');
    
//     Route::get('/admin/web-request', function () {
//         return view('web-request');
//     })->name('admin.web-request');


});


// Route::get('/test',function ()
// {
//     $response = Http::get('https://dev.farizdotid.com/api/daerahindonesia/provinsi');
//     $data['provinsi'] = $response->json();
//    return view('landing_v2',$data);
// });

Route::get('/daftar', [LandingPages::class,'index'])->name('landing');
Route::get('/', [LandingPages::class,'home'])->name('landing.index');
Route::get('/wa-broadcast', [LandingPages::class,'broadcast'])->name('landing.broadcast');


    Route::group(['prefix'=>'get-api'],function ()
    {
        Route::get('/getKota/{id}', [LandingPages::class,'getKota'])->name('getKota');
        Route::get('/getKec/{id}', [LandingPages::class,'getKec'])->name('getKec');
        Route::get('/getKel/{id}', [LandingPages::class,'getKel'])->name('getKel');
        Route::post('/simpan-pendaftaran', [LandingPages::class,'simpanPendaftaran'])->name('simpanPendaftaran');
    });



    Route::get('/logout', function () {
        Auth::logout();
    });


Auth::routes();

Route::middleware(['auth','user-access:user'])->group(function(){
    Route::get('/claim', [App\Http\Controllers\HomeController::class, 'claim'])->name('claim');
    Route::post('/claim/store', [App\Http\Controllers\HomeController::class, 'store'])->name('claim.store');
    Route::post('/claim/store_prem', [App\Http\Controllers\HomeController::class, 'storePrem'])->name('claim.storePrem');
    Route::post('/claim/brosur', [App\Http\Controllers\HomeController::class, 'brosur'])->name('claim.brosur');
    Route::post('/claim/ppob', [App\Http\Controllers\HomeController::class, 'ppob'])->name('claim.ppob');
    Route::post('/claim/merchan', [App\Http\Controllers\HomeController::class, 'merchan'])->name('claim.merchan');
    Route::post('/claim/webinar', [App\Http\Controllers\HomeController::class, 'webinar'])->name('claim.webinar');
    Route::post('/claim/program-menarik', [App\Http\Controllers\HomeController::class, 'programMenarik'])->name('claim.programMenarik');
    Route::get('/claim/getJenis/', [App\Http\Controllers\HomeController::class, 'getJenis'])->name('getJenis');
    Route::get('/claim/getTema/', [App\Http\Controllers\HomeController::class, 'getTema'])->name('getTema');
    Route::get('/home', function ()
    {
        return redirect()->route('claim');
    })->name('home');
    
});


Route::get('/send-email/', [EmailController::class, 'sendWelcomeEmail']);